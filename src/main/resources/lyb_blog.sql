/*
Navicat MySQL Data Transfer

Source Server         : 本机mysql
Source Server Version : 50016
Source Host           : localhost:3306
Source Database       : lyb_blog

Target Server Type    : MYSQL
Target Server Version : 50016
File Encoding         : 65001

Date: 2018-05-06 13:55:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `loginName` varchar(255) collate utf8_bin NOT NULL,
  `loginPass` varchar(255) collate utf8_bin default NULL,
  `type` int(4) NOT NULL,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('4af31e5ccbb445ec98c8e950d44b84e6', 'admin', '123456', '1');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `isPrivate` tinyint(4) default NULL,
  `createTime` bigint(20) default NULL,
  `lastEditTime` bigint(20) default NULL,
  `authorUuid` varchar(32) collate utf8_bin default NULL,
  `typeUuid` varchar(32) collate utf8_bin default NULL,
  `title` varchar(255) collate utf8_bin default NULL,
  `content` text collate utf8_bin,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1617d8a9088b4a9eb9bd5c8042b09de3', '1', '1525153434884', null, 'd2327e6c157a4b0fb065dada87c72d7e', '6269659576e34ca39318143056c6e845', '煞笔', 0x3C703EE591B5E591B5E593923C2F703E3C703E3C62722F3E3C2F703E3C703EE4BDA0E5A6B9E79A84266E6273703B266E6273703B3C2F703E3C703E3C62722F3E3C2F703E3C703EE7A59EE7BB8FE797853C2F703E);
INSERT INTO `article` VALUES ('25e2cb4b1cc04086a9aefc1a1e98d323', '1', '1525152790354', null, 'd2327e6c157a4b0fb065dada87c72d7e', 'a864138e0eb248eb88f1d6df3c2febfb', null, 0x3C703EE59CA8E8BF99E9878CE7BC96E8BE91E69687E7ABA0E58685E5AEB93C2F703E);
INSERT INTO `article` VALUES ('42c8ab792bad41f386660d70373a5cfa', '1', '1525153000416', null, 'd2327e6c157a4b0fb065dada87c72d7e', 'a864138e0eb248eb88f1d6df3c2febfb', null, 0x3C703E363636E7A781E69C89E79A8420E699BAE99A9CE6B283E789B93C2F703E);
INSERT INTO `article` VALUES ('45582b4a70054fe2995f052f1249281f', '1', '1525575475233', null, 'd2327e6c157a4b0fb065dada87c72d7e', 'a864138e0eb248eb88f1d6df3c2febfb', 'asdf', 0x3C703E3C696D67207372633D22687474703A2F2F696D672E62616964752E636F6D2F68692F6A78322F6A5F303030312E676966222F3EE59CA8E8BF99E9878CE7BC96E8BE91E69687E7ABA0E58685E5AEB93C696D67207372633D22687474703A2F2F696D672E62616964752E636F6D2F68692F6A78322F6A5F303030312E676966222F3E3C2F703E3C703E3C62722F3E3C2F703E3C703E3C696D67207372633D222F75706C6F61642F696D6167652F32303138303530362F313532353537353433323630313036323730362E6A706722207469746C653D22313532353537353433323630313036323730362E6A70672220616C743D2257494E5F32303138303433305F31315F30365F31385F50726F2E6A7067222077696474683D2233383622206865696768743D22333039222F3E3C2F703E);
INSERT INTO `article` VALUES ('59dcbf6d710a48749313c2841517b995', '0', '1525153164369', null, 'd2327e6c157a4b0fb065dada87c72d7e', '5783f8a2ce9e4bd3b3254677121c4ad7', '傻逼吧', 0x3C703EE59CA8E8BF99E9878CE7BC96E8BE91E69687E7ABA0E58685E5AEB97373733C2F703E);
INSERT INTO `article` VALUES ('b1bc813b652f4585a6b41f6aa3afddbf', '1', '1525152870197', null, 'd2327e6c157a4b0fb065dada87c72d7e', '5783f8a2ce9e4bd3b3254677121c4ad7', null, 0x3C703EE59CA8E8BF99E9878CE7BC96E8BE91E69687E7ABA0E58685E5AEB97364667364663C2F703E);

-- ----------------------------
-- Table structure for articlecontenttype
-- ----------------------------
DROP TABLE IF EXISTS `articlecontenttype`;
CREATE TABLE `articlecontenttype` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `typeName` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of articlecontenttype
-- ----------------------------

-- ----------------------------
-- Table structure for author
-- ----------------------------
DROP TABLE IF EXISTS `author`;
CREATE TABLE `author` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `loginPass` varchar(255) collate utf8_bin default NULL,
  `loginName` varchar(255) collate utf8_bin default NULL,
  `stat` int(11) default NULL,
  `levelUuid` varchar(32) collate utf8_bin default NULL,
  `nickName` varchar(255) collate utf8_bin default NULL,
  `sex` tinyint(4) default NULL,
  `headImg` varchar(255) collate utf8_bin default NULL,
  `phone` varchar(255) collate utf8_bin default NULL,
  `age` int(11) default NULL,
  `address` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`uuid`),
  KEY `级别表` (`levelUuid`),
  CONSTRAINT `级别表` FOREIGN KEY (`levelUuid`) REFERENCES `level` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of author
-- ----------------------------
INSERT INTO `author` VALUES ('5c4d9138821a4e0297313100d1bd83a4', '123456', '1570194845@qq.com', null, '1c1885430f644afb8235830939b721e2', '1570194845@qq.com', null, null, null, null, null);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `content` text collate utf8_bin,
  `fromAuthor` varchar(32) collate utf8_bin default NULL,
  `createTime` bigint(20) default NULL,
  `articleUuid` varchar(32) collate utf8_bin default NULL,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for friendlink
-- ----------------------------
DROP TABLE IF EXISTS `friendlink`;
CREATE TABLE `friendlink` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `href` varchar(255) collate utf8_bin default NULL,
  `name` varchar(255) collate utf8_bin default NULL,
  `stat` int(11) default NULL,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of friendlink
-- ----------------------------

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `levelNum` int(11) default NULL,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of level
-- ----------------------------
INSERT INTO `level` VALUES ('1c1885430f644afb8235830939b721e2', '1');

-- ----------------------------
-- Table structure for swipers
-- ----------------------------
DROP TABLE IF EXISTS `swipers`;
CREATE TABLE `swipers` (
  `uuid` varchar(32) collate utf8_bin NOT NULL,
  `href` varchar(255) collate utf8_bin default NULL,
  `title` varchar(255) collate utf8_bin default NULL,
  `imgAddress` varchar(255) collate utf8_bin default NULL,
  `stat` int(11) default NULL,
  `sequence` int(11) default NULL,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of swipers
-- ----------------------------

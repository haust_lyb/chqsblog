import aop.ScanPoint_Aop;
import controller.ScanPoint_Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;
import org.springframework.context.annotation.*;
import org.springframework.mail.MailSender;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;
import service.ScanPoint_Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
@Import({WebSocketConfig.class,MybatisConfig.class, BeansConfig.class, MailConfig.class})
@ComponentScan(basePackageClasses = {ScanPoint_Service.class, ScanPoint_Controller.class, ScanPoint_Aop.class}, excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class)})
public class RootConfig extends WebMvcConfigurerAdapter {

//    @Bean
//    public ViewResolver viewResolver() {//因为我们是多个视图解析器，所以在下边统一配置
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//        resolver.setPrefix("/WEB-INF/");
//        //resolver.setSuffix(".jsp");
//        resolver.setExposeContextBeansAsAttributes(true);
//        resolver.setOrder(2);
//        resolver.setViewNames("*.jsp");
//        return resolver;
//    }

    @Bean
    public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
        ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
        ArrayList<ViewResolver> list = new ArrayList<ViewResolver>();

        InternalResourceViewResolver jspResolver = new InternalResourceViewResolver();
        jspResolver.setPrefix("/WEB-INF/views/");
        jspResolver.setExposeContextBeansAsAttributes(true);
        jspResolver.setOrder(3);
        jspResolver.setViewNames("*.jsp");

//        ThymeleafViewResolver thymeleafViewResolver=new ThymeleafViewResolver();
//        thymeleafViewResolver.setCharacterEncoding("UTF-8");
//        thymeleafViewResolver.setTemplateEngine(templateEngine());
//        thymeleafViewResolver.setViewNames(new String[]{"*.html"});
//        thymeleafViewResolver.setOrder(2);

        ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
        thymeleafViewResolver.setCharacterEncoding("UTF-8");
        HashSet set = new HashSet();
        set.add(emailTemplateResolver());
        set.add(templateResolver());
        thymeleafViewResolver.setTemplateEngine(templateEngine(set));
        thymeleafViewResolver.setViewNames(new String[]{"*.html"});
        thymeleafViewResolver.setCharacterEncoding("UTF-8");


        list.add(jspResolver);
//        list.add(thymeleafViewResolver);
        list.add(thymeleafViewResolver);
        contentNegotiatingViewResolver.setViewResolvers(list);
        return contentNegotiatingViewResolver;
    }


//    @Bean
//    public SpringTemplateEngine templateEngine(TemplateResolver templateResolver){
//        //thymeleaf模板引擎
//        SpringTemplateEngine templateEngine=new SpringTemplateEngine();
//        templateEngine.setTemplateResolver(templateResolver);
//        return templateEngine;
//    }

    @Bean
    public SpringTemplateEngine templateEngine(Set<ITemplateResolver> templateResolvers) {
        //thymeleaf模板引擎
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolvers(templateResolvers);
        return templateEngine;
    }

    @Bean
    public TemplateResolver templateResolver() {
        //thymeleaf模板解析器
        TemplateResolver templateResolver = new ServletContextTemplateResolver();
        templateResolver.setPrefix("/WEB-INF/views/");
        templateResolver.setTemplateMode("HTML5");
        templateResolver.setCacheable(false);
        //templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setOrder(2);
        return templateResolver;
    }

    @Bean
    public ClassLoaderTemplateResolver emailTemplateResolver() {
        //thymeleaf模板解析器
        ClassLoaderTemplateResolver emailTemplateResolver = new ClassLoaderTemplateResolver();
        emailTemplateResolver.setPrefix("classpathThymeleafTemplate/");
        emailTemplateResolver.setTemplateMode("HTML5");
        emailTemplateResolver.setCacheable(false);
        //emailTemplateResolver.setCharacterEncoding("UTF-8");
        emailTemplateResolver.setOrder(1);
        return emailTemplateResolver;
    }

    //    @Bean
//    public ViewResolver viewResolver(SpringTemplateEngine templateEngine){
//        //Thymeleaf视图解析器
//        ThymeleafViewResolver viewResolver=new ThymeleafViewResolver();
//        viewResolver.setTemplateEngine(templateEngine);
//        return viewResolver;
//    }


//    @Bean
//    public Logger getLogger(){//如何正常的配置一个Logger日志？待解决
//        return LoggerFactory.getLogger(RootConfig.class);
//    }


    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        //这个应该是静态资源的处理方式吧。。。
        configurer.enable();
    }


}

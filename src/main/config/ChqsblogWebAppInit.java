import com.opensymphony.sitemesh.webapp.SiteMeshFilter;
import filters.MyFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
public class ChqsblogWebAppInit extends AbstractAnnotationConfigDispatcherServletInitializer {

    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ChqsblogWebAppInit.class};
    }

    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{RootConfig.class};
    }

    protected String[] getServletMappings() {
        return new String[]{"/"};
    }


//    public void onStartup(ServletContext servletContext) throws ServletException {
//        FilterRegistration.Dynamic filter=servletContext.addFilter("myFilter", MyFilter.class);//注册Filter
//
//        filter.addMappingForUrlPatterns(null,false,"/front/*");//添加Filter的映射路径
//    }


    @Override
    protected Filter[] getServletFilters() {//配置我的过滤器 （CharacterEncodingFilter 可以解决乱码问题）
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return new Filter[]{new MyFilter(), characterEncodingFilter,new SiteMeshFilter()};
    }


//    @Override
//    protected Filter[] getServletFilters() {
//        return new Filter[]{new MyFilter(), new CharacterEncodingFilter("UTF-8",true)};
//    }
}

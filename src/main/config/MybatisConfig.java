import com.alibaba.druid.pool.DruidDataSource;
import mybatis.ScanPoint_Mapper;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * Created by Liyibo on 2018/4/9.
 */
@Configuration
@PropertySource("classpath:/generator.properties")
@MapperScan(basePackages="mybatis.mapper")
@ComponentScan(basePackageClasses = ScanPoint_Mapper.class)
public class MybatisConfig {


    @Autowired
    Environment env;

    @Bean(name="dataSource")
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClass"));
        dataSource.setUrl(env.getProperty("jdbc.connectionURL"));
        dataSource.setUsername(env.getProperty("jdbc.userId"));
        dataSource.setPassword(env.getProperty("jdbc.password"));
        return dataSource;
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) throws IOException {
        //ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        return sqlSessionFactoryBean;
    }


}

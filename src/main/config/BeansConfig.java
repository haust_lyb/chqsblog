import com.lyb.utils.LybPassWordEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.IOException;

/**
 * Created by Liyibo on 2018/4/9.
 */
@Configuration
public class BeansConfig {

    @Bean
    public LybPassWordEncoder lybPassWordEncoder(){//配置自定义的密码转码器
        LybPassWordEncoder lybPassWordEncoder = new LybPassWordEncoder(5,"1570194845@qq.com");
        return lybPassWordEncoder;
    }
}

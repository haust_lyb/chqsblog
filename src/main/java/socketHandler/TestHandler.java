package socketHandler;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;

public class TestHandler extends AbstractWebSocketHandler {
    private static ArrayList<WebSocketSession> users;
    static {
        users = new ArrayList<WebSocketSession>();
    }
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String khd=message.getPayload();
        System.out.println(message.getPayload());
        sendMessageToUsers(new TextMessage(khd));
    }


    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println(session.getId()+"登陆了");
        users.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        System.out.println(session.getId()+"退出了");
        users.remove(session);
    }


    public void sendMessageToUsers(TextMessage message) {
        System.out.println(message);
        for (WebSocketSession user : users) {
            try {
                if (user.isOpen()) {
                    user.sendMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

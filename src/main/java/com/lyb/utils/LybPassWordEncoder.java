package com.lyb.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Liyibo on 2018/4/24.
 */
public class LybPassWordEncoder{
    private int times;//加密次数
    private String simpleSalt;//加密的默认的盐

    public LybPassWordEncoder(){};
    public LybPassWordEncoder(int times){
        this.times=times;
    };
    public LybPassWordEncoder(String simpleSalt){
        this.simpleSalt=simpleSalt;
    }
    public LybPassWordEncoder(int times,String simpleSalt){
        this.times=times;
        this.simpleSalt=simpleSalt;
    };
    public String encode(CharSequence charSequence) {

        return md5str(charSequence.toString(),simpleSalt);
    }

    public boolean matches(CharSequence charSequence, String s) {
        return this.encode(charSequence).equals(s);
    }

    private String md5str(String s,String simpleSalt){
        String finalStr=s;

        if (s==null){
            throw  new RuntimeException("传入的加密字符串对象不能为空");
        }else {
            if (simpleSalt==null){
                for (int i = 0; i <= times; i++) {
                    finalStr=DigestUtils.md5Hex(finalStr);
                }
            }else {
                for (int i = 0; i <= times; i++) {
                    finalStr=DigestUtils.md5Hex(finalStr+simpleSalt);
                }
            }

        }
        return finalStr;
    }

}

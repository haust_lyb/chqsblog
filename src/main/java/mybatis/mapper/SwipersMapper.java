package mybatis.mapper;

import java.util.List;
import mybatis.model.Swipers;
import mybatis.model.SwipersExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface SwipersMapper {
    @SelectProvider(type=SwipersSqlProvider.class, method="countByExample")
    int countByExample(SwipersExample example);

    @DeleteProvider(type=SwipersSqlProvider.class, method="deleteByExample")
    int deleteByExample(SwipersExample example);

    @Delete({
        "delete from swipers",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into swipers (uuid, href, ",
        "title, imgAddress, ",
        "stat, sequence)",
        "values (#{uuid,jdbcType=VARCHAR}, #{href,jdbcType=VARCHAR}, ",
        "#{title,jdbcType=VARCHAR}, #{imgaddress,jdbcType=VARCHAR}, ",
        "#{stat,jdbcType=INTEGER}, #{sequence,jdbcType=INTEGER})"
    })
    int insert(Swipers record);

    @InsertProvider(type=SwipersSqlProvider.class, method="insertSelective")
    int insertSelective(Swipers record);

    @SelectProvider(type=SwipersSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="href", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="title", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="imgAddress", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="stat", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="sequence", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    List<Swipers> selectByExample(SwipersExample example);

    @Select({
        "select",
        "uuid, href, title, imgAddress, stat, sequence",
        "from swipers",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="href", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="title", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="imgAddress", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="stat", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="sequence", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    Swipers selectByPrimaryKey(String uuid);

    @UpdateProvider(type=SwipersSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Swipers record, @Param("example") SwipersExample example);

    @UpdateProvider(type=SwipersSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Swipers record, @Param("example") SwipersExample example);

    @UpdateProvider(type=SwipersSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Swipers record);

    @Update({
        "update swipers",
        "set href = #{href,jdbcType=VARCHAR},",
          "title = #{title,jdbcType=VARCHAR},",
          "imgAddress = #{imgaddress,jdbcType=VARCHAR},",
          "stat = #{stat,jdbcType=INTEGER},",
          "sequence = #{sequence,jdbcType=INTEGER}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Swipers record);
}
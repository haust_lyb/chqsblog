package mybatis.mapper;

import java.util.List;
import mybatis.model.Comment;
import mybatis.model.CommentExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentMapper {
    @SelectProvider(type=CommentSqlProvider.class, method="countByExample")
    int countByExample(CommentExample example);

    @DeleteProvider(type=CommentSqlProvider.class, method="deleteByExample")
    int deleteByExample(CommentExample example);

    @Delete({
        "delete from comment",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into comment (uuid, fromAuthor, ",
        "createTime, articleUuid, ",
        "content)",
        "values (#{uuid,jdbcType=VARCHAR}, #{fromauthor,jdbcType=VARCHAR}, ",
        "#{createtime,jdbcType=BIGINT}, #{articleuuid,jdbcType=VARCHAR}, ",
        "#{content,jdbcType=LONGVARCHAR})"
    })
    int insert(Comment record);

    @InsertProvider(type=CommentSqlProvider.class, method="insertSelective")
    int insertSelective(Comment record);

    @SelectProvider(type=CommentSqlProvider.class, method="selectByExampleWithBLOBs")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="fromAuthor", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="createTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="articleUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="content", javaType=String.class, jdbcType=JdbcType.LONGVARCHAR)
    })
    List<Comment> selectByExampleWithBLOBs(CommentExample example);

    @SelectProvider(type=CommentSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="fromAuthor", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="createTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="articleUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR)
    })
    List<Comment> selectByExample(CommentExample example);

    @Select({
        "select",
        "uuid, fromAuthor, createTime, articleUuid, content",
        "from comment",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="fromAuthor", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="createTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="articleUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="content", javaType=String.class, jdbcType=JdbcType.LONGVARCHAR)
    })
    Comment selectByPrimaryKey(String uuid);

    @UpdateProvider(type=CommentSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Comment record, @Param("example") CommentExample example);

    @UpdateProvider(type=CommentSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") Comment record, @Param("example") CommentExample example);

    @UpdateProvider(type=CommentSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Comment record, @Param("example") CommentExample example);

    @UpdateProvider(type=CommentSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Comment record);

    @Update({
        "update comment",
        "set fromAuthor = #{fromauthor,jdbcType=VARCHAR},",
          "createTime = #{createtime,jdbcType=BIGINT},",
          "articleUuid = #{articleuuid,jdbcType=VARCHAR},",
          "content = #{content,jdbcType=LONGVARCHAR}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKeyWithBLOBs(Comment record);

    @Update({
        "update comment",
        "set fromAuthor = #{fromauthor,jdbcType=VARCHAR},",
          "createTime = #{createtime,jdbcType=BIGINT},",
          "articleUuid = #{articleuuid,jdbcType=VARCHAR}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Comment record);
}
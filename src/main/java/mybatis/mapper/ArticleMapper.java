package mybatis.mapper;

import java.util.List;
import mybatis.model.Article;
import mybatis.model.ArticleExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleMapper {
    @SelectProvider(type=ArticleSqlProvider.class, method="countByExample")
    int countByExample(ArticleExample example);

    @DeleteProvider(type=ArticleSqlProvider.class, method="deleteByExample")
    int deleteByExample(ArticleExample example);

    @Delete({
        "delete from article",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into article (uuid, isPrivate, ",
        "createTime, lastEditTime, ",
        "authorUuid, typeUuid, ",
        "description, title, ",
        "content)",
        "values (#{uuid,jdbcType=VARCHAR}, #{isprivate,jdbcType=INTEGER}, ",
        "#{createtime,jdbcType=BIGINT}, #{lastedittime,jdbcType=BIGINT}, ",
        "#{authoruuid,jdbcType=VARCHAR}, #{typeuuid,jdbcType=VARCHAR}, ",
        "#{description,jdbcType=VARCHAR}, #{title,jdbcType=VARCHAR}, ",
        "#{content,jdbcType=LONGVARCHAR})"
    })
    int insert(Article record);

    @InsertProvider(type=ArticleSqlProvider.class, method="insertSelective")
    int insertSelective(Article record);

    @SelectProvider(type=ArticleSqlProvider.class, method="selectByExampleWithBLOBs")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="isPrivate", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="createTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="lastEditTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="authorUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="typeUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="description", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="title", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="content", javaType=String.class, jdbcType=JdbcType.LONGVARCHAR)
    })
    List<Article> selectByExampleWithBLOBs(ArticleExample example);

    @SelectProvider(type=ArticleSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="isPrivate", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="createTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="lastEditTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="authorUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="typeUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="description", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="title", javaType=String.class, jdbcType=JdbcType.VARCHAR)
    })
    List<Article> selectByExample(ArticleExample example);

    @Select({
        "select",
        "uuid, isPrivate, createTime, lastEditTime, authorUuid, typeUuid, description, ",
        "title, content",
        "from article",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="isPrivate", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="createTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="lastEditTime", javaType=Long.class, jdbcType=JdbcType.BIGINT),
        @Arg(column="authorUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="typeUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="description", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="title", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="content", javaType=String.class, jdbcType=JdbcType.LONGVARCHAR)
    })
    Article selectByPrimaryKey(String uuid);

    @UpdateProvider(type=ArticleSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Article record, @Param("example") ArticleExample example);

    @UpdateProvider(type=ArticleSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") Article record, @Param("example") ArticleExample example);

    @UpdateProvider(type=ArticleSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Article record, @Param("example") ArticleExample example);

    @UpdateProvider(type=ArticleSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Article record);

    @Update({
        "update article",
        "set isPrivate = #{isprivate,jdbcType=INTEGER},",
          "createTime = #{createtime,jdbcType=BIGINT},",
          "lastEditTime = #{lastedittime,jdbcType=BIGINT},",
          "authorUuid = #{authoruuid,jdbcType=VARCHAR},",
          "typeUuid = #{typeuuid,jdbcType=VARCHAR},",
          "description = #{description,jdbcType=VARCHAR},",
          "title = #{title,jdbcType=VARCHAR},",
          "content = #{content,jdbcType=LONGVARCHAR}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKeyWithBLOBs(Article record);

    @Update({
        "update article",
        "set isPrivate = #{isprivate,jdbcType=INTEGER},",
          "createTime = #{createtime,jdbcType=BIGINT},",
          "lastEditTime = #{lastedittime,jdbcType=BIGINT},",
          "authorUuid = #{authoruuid,jdbcType=VARCHAR},",
          "typeUuid = #{typeuuid,jdbcType=VARCHAR},",
          "description = #{description,jdbcType=VARCHAR},",
          "title = #{title,jdbcType=VARCHAR}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Article record);
}
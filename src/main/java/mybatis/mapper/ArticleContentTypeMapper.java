package mybatis.mapper;

import java.util.List;
import mybatis.model.ArticleContentType;
import mybatis.model.ArticleContentTypeExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleContentTypeMapper {
    @SelectProvider(type=ArticleContentTypeSqlProvider.class, method="countByExample")
    int countByExample(ArticleContentTypeExample example);

    @DeleteProvider(type=ArticleContentTypeSqlProvider.class, method="deleteByExample")
    int deleteByExample(ArticleContentTypeExample example);

    @Delete({
        "delete from articlecontenttype",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into articlecontenttype (uuid, typeName)",
        "values (#{uuid,jdbcType=VARCHAR}, #{typename,jdbcType=VARCHAR})"
    })
    int insert(ArticleContentType record);

    @InsertProvider(type=ArticleContentTypeSqlProvider.class, method="insertSelective")
    int insertSelective(ArticleContentType record);

    @SelectProvider(type=ArticleContentTypeSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="typeName", javaType=String.class, jdbcType=JdbcType.VARCHAR)
    })
    List<ArticleContentType> selectByExample(ArticleContentTypeExample example);

    @Select({
        "select",
        "uuid, typeName",
        "from articlecontenttype",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="typeName", javaType=String.class, jdbcType=JdbcType.VARCHAR)
    })
    ArticleContentType selectByPrimaryKey(String uuid);

    @UpdateProvider(type=ArticleContentTypeSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") ArticleContentType record, @Param("example") ArticleContentTypeExample example);

    @UpdateProvider(type=ArticleContentTypeSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") ArticleContentType record, @Param("example") ArticleContentTypeExample example);

    @UpdateProvider(type=ArticleContentTypeSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(ArticleContentType record);

    @Update({
        "update articlecontenttype",
        "set typeName = #{typename,jdbcType=VARCHAR}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(ArticleContentType record);
}
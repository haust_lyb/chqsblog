package mybatis.mapper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import java.util.List;
import java.util.Map;
import mybatis.model.Author;
import mybatis.model.AuthorExample.Criteria;
import mybatis.model.AuthorExample.Criterion;
import mybatis.model.AuthorExample;

public class AuthorSqlProvider {

    public String countByExample(AuthorExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("author");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(AuthorExample example) {
        BEGIN();
        DELETE_FROM("author");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(Author record) {
        BEGIN();
        INSERT_INTO("author");
        
        if (record.getUuid() != null) {
            VALUES("uuid", "#{uuid,jdbcType=VARCHAR}");
        }
        
        if (record.getLoginpass() != null) {
            VALUES("loginPass", "#{loginpass,jdbcType=VARCHAR}");
        }
        
        if (record.getLoginname() != null) {
            VALUES("loginName", "#{loginname,jdbcType=VARCHAR}");
        }
        
        if (record.getStat() != null) {
            VALUES("stat", "#{stat,jdbcType=INTEGER}");
        }
        
        if (record.getLeveluuid() != null) {
            VALUES("levelUuid", "#{leveluuid,jdbcType=VARCHAR}");
        }
        
        if (record.getNickname() != null) {
            VALUES("nickName", "#{nickname,jdbcType=VARCHAR}");
        }
        
        if (record.getSex() != null) {
            VALUES("sex", "#{sex,jdbcType=TINYINT}");
        }
        
        if (record.getHeadimg() != null) {
            VALUES("headImg", "#{headimg,jdbcType=VARCHAR}");
        }
        
        if (record.getPhone() != null) {
            VALUES("phone", "#{phone,jdbcType=VARCHAR}");
        }
        
        if (record.getAge() != null) {
            VALUES("age", "#{age,jdbcType=INTEGER}");
        }
        
        if (record.getAddress() != null) {
            VALUES("address", "#{address,jdbcType=VARCHAR}");
        }
        
        return SQL();
    }

    public String selectByExample(AuthorExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("uuid");
        } else {
            SELECT("uuid");
        }
        SELECT("loginPass");
        SELECT("loginName");
        SELECT("stat");
        SELECT("levelUuid");
        SELECT("nickName");
        SELECT("sex");
        SELECT("headImg");
        SELECT("phone");
        SELECT("age");
        SELECT("address");
        FROM("author");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        Author record = (Author) parameter.get("record");
        AuthorExample example = (AuthorExample) parameter.get("example");
        
        BEGIN();
        UPDATE("author");
        
        if (record.getUuid() != null) {
            SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        }
        
        if (record.getLoginpass() != null) {
            SET("loginPass = #{record.loginpass,jdbcType=VARCHAR}");
        }
        
        if (record.getLoginname() != null) {
            SET("loginName = #{record.loginname,jdbcType=VARCHAR}");
        }
        
        if (record.getStat() != null) {
            SET("stat = #{record.stat,jdbcType=INTEGER}");
        }
        
        if (record.getLeveluuid() != null) {
            SET("levelUuid = #{record.leveluuid,jdbcType=VARCHAR}");
        }
        
        if (record.getNickname() != null) {
            SET("nickName = #{record.nickname,jdbcType=VARCHAR}");
        }
        
        if (record.getSex() != null) {
            SET("sex = #{record.sex,jdbcType=TINYINT}");
        }
        
        if (record.getHeadimg() != null) {
            SET("headImg = #{record.headimg,jdbcType=VARCHAR}");
        }
        
        if (record.getPhone() != null) {
            SET("phone = #{record.phone,jdbcType=VARCHAR}");
        }
        
        if (record.getAge() != null) {
            SET("age = #{record.age,jdbcType=INTEGER}");
        }
        
        if (record.getAddress() != null) {
            SET("address = #{record.address,jdbcType=VARCHAR}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("author");
        
        SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        SET("loginPass = #{record.loginpass,jdbcType=VARCHAR}");
        SET("loginName = #{record.loginname,jdbcType=VARCHAR}");
        SET("stat = #{record.stat,jdbcType=INTEGER}");
        SET("levelUuid = #{record.leveluuid,jdbcType=VARCHAR}");
        SET("nickName = #{record.nickname,jdbcType=VARCHAR}");
        SET("sex = #{record.sex,jdbcType=TINYINT}");
        SET("headImg = #{record.headimg,jdbcType=VARCHAR}");
        SET("phone = #{record.phone,jdbcType=VARCHAR}");
        SET("age = #{record.age,jdbcType=INTEGER}");
        SET("address = #{record.address,jdbcType=VARCHAR}");
        
        AuthorExample example = (AuthorExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(Author record) {
        BEGIN();
        UPDATE("author");
        
        if (record.getLoginpass() != null) {
            SET("loginPass = #{loginpass,jdbcType=VARCHAR}");
        }
        
        if (record.getLoginname() != null) {
            SET("loginName = #{loginname,jdbcType=VARCHAR}");
        }
        
        if (record.getStat() != null) {
            SET("stat = #{stat,jdbcType=INTEGER}");
        }
        
        if (record.getLeveluuid() != null) {
            SET("levelUuid = #{leveluuid,jdbcType=VARCHAR}");
        }
        
        if (record.getNickname() != null) {
            SET("nickName = #{nickname,jdbcType=VARCHAR}");
        }
        
        if (record.getSex() != null) {
            SET("sex = #{sex,jdbcType=TINYINT}");
        }
        
        if (record.getHeadimg() != null) {
            SET("headImg = #{headimg,jdbcType=VARCHAR}");
        }
        
        if (record.getPhone() != null) {
            SET("phone = #{phone,jdbcType=VARCHAR}");
        }
        
        if (record.getAge() != null) {
            SET("age = #{age,jdbcType=INTEGER}");
        }
        
        if (record.getAddress() != null) {
            SET("address = #{address,jdbcType=VARCHAR}");
        }
        
        WHERE("uuid = #{uuid,jdbcType=VARCHAR}");
        
        return SQL();
    }

    protected void applyWhere(AuthorExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}
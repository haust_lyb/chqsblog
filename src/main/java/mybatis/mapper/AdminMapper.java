package mybatis.mapper;

import java.util.List;
import mybatis.model.Admin;
import mybatis.model.AdminExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper {
    @SelectProvider(type=AdminSqlProvider.class, method="countByExample")
    int countByExample(AdminExample example);

    @DeleteProvider(type=AdminSqlProvider.class, method="deleteByExample")
    int deleteByExample(AdminExample example);

    @Delete({
        "delete from admin",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into admin (uuid, loginName, ",
        "loginPass, type)",
        "values (#{uuid,jdbcType=VARCHAR}, #{loginname,jdbcType=VARCHAR}, ",
        "#{loginpass,jdbcType=VARCHAR}, #{type,jdbcType=INTEGER})"
    })
    int insert(Admin record);

    @InsertProvider(type=AdminSqlProvider.class, method="insertSelective")
    int insertSelective(Admin record);

    @SelectProvider(type=AdminSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="loginName", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="loginPass", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="type", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    List<Admin> selectByExample(AdminExample example);

    @Select({
        "select",
        "uuid, loginName, loginPass, type",
        "from admin",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="loginName", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="loginPass", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="type", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    Admin selectByPrimaryKey(String uuid);

    @UpdateProvider(type=AdminSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Admin record, @Param("example") AdminExample example);

    @UpdateProvider(type=AdminSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Admin record, @Param("example") AdminExample example);

    @UpdateProvider(type=AdminSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Admin record);

    @Update({
        "update admin",
        "set loginName = #{loginname,jdbcType=VARCHAR},",
          "loginPass = #{loginpass,jdbcType=VARCHAR},",
          "type = #{type,jdbcType=INTEGER}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Admin record);
}
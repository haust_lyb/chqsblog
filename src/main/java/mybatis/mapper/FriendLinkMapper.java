package mybatis.mapper;

import java.util.List;
import mybatis.model.FriendLink;
import mybatis.model.FriendLinkExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendLinkMapper {
    @SelectProvider(type=FriendLinkSqlProvider.class, method="countByExample")
    int countByExample(FriendLinkExample example);

    @DeleteProvider(type=FriendLinkSqlProvider.class, method="deleteByExample")
    int deleteByExample(FriendLinkExample example);

    @Delete({
        "delete from friendlink",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into friendlink (uuid, href, ",
        "name, stat)",
        "values (#{uuid,jdbcType=VARCHAR}, #{href,jdbcType=VARCHAR}, ",
        "#{name,jdbcType=VARCHAR}, #{stat,jdbcType=INTEGER})"
    })
    int insert(FriendLink record);

    @InsertProvider(type=FriendLinkSqlProvider.class, method="insertSelective")
    int insertSelective(FriendLink record);

    @SelectProvider(type=FriendLinkSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="href", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="name", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="stat", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    List<FriendLink> selectByExample(FriendLinkExample example);

    @Select({
        "select",
        "uuid, href, name, stat",
        "from friendlink",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="href", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="name", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="stat", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    FriendLink selectByPrimaryKey(String uuid);

    @UpdateProvider(type=FriendLinkSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") FriendLink record, @Param("example") FriendLinkExample example);

    @UpdateProvider(type=FriendLinkSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") FriendLink record, @Param("example") FriendLinkExample example);

    @UpdateProvider(type=FriendLinkSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(FriendLink record);

    @Update({
        "update friendlink",
        "set href = #{href,jdbcType=VARCHAR},",
          "name = #{name,jdbcType=VARCHAR},",
          "stat = #{stat,jdbcType=INTEGER}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(FriendLink record);
}
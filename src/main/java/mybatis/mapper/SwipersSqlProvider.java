package mybatis.mapper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import java.util.List;
import java.util.Map;
import mybatis.model.Swipers;
import mybatis.model.SwipersExample.Criteria;
import mybatis.model.SwipersExample.Criterion;
import mybatis.model.SwipersExample;

public class SwipersSqlProvider {

    public String countByExample(SwipersExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("swipers");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(SwipersExample example) {
        BEGIN();
        DELETE_FROM("swipers");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(Swipers record) {
        BEGIN();
        INSERT_INTO("swipers");
        
        if (record.getUuid() != null) {
            VALUES("uuid", "#{uuid,jdbcType=VARCHAR}");
        }
        
        if (record.getHref() != null) {
            VALUES("href", "#{href,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            VALUES("title", "#{title,jdbcType=VARCHAR}");
        }
        
        if (record.getImgaddress() != null) {
            VALUES("imgAddress", "#{imgaddress,jdbcType=VARCHAR}");
        }
        
        if (record.getStat() != null) {
            VALUES("stat", "#{stat,jdbcType=INTEGER}");
        }
        
        if (record.getSequence() != null) {
            VALUES("sequence", "#{sequence,jdbcType=INTEGER}");
        }
        
        return SQL();
    }

    public String selectByExample(SwipersExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("uuid");
        } else {
            SELECT("uuid");
        }
        SELECT("href");
        SELECT("title");
        SELECT("imgAddress");
        SELECT("stat");
        SELECT("sequence");
        FROM("swipers");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        Swipers record = (Swipers) parameter.get("record");
        SwipersExample example = (SwipersExample) parameter.get("example");
        
        BEGIN();
        UPDATE("swipers");
        
        if (record.getUuid() != null) {
            SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        }
        
        if (record.getHref() != null) {
            SET("href = #{record.href,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{record.title,jdbcType=VARCHAR}");
        }
        
        if (record.getImgaddress() != null) {
            SET("imgAddress = #{record.imgaddress,jdbcType=VARCHAR}");
        }
        
        if (record.getStat() != null) {
            SET("stat = #{record.stat,jdbcType=INTEGER}");
        }
        
        if (record.getSequence() != null) {
            SET("sequence = #{record.sequence,jdbcType=INTEGER}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("swipers");
        
        SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        SET("href = #{record.href,jdbcType=VARCHAR}");
        SET("title = #{record.title,jdbcType=VARCHAR}");
        SET("imgAddress = #{record.imgaddress,jdbcType=VARCHAR}");
        SET("stat = #{record.stat,jdbcType=INTEGER}");
        SET("sequence = #{record.sequence,jdbcType=INTEGER}");
        
        SwipersExample example = (SwipersExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(Swipers record) {
        BEGIN();
        UPDATE("swipers");
        
        if (record.getHref() != null) {
            SET("href = #{href,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{title,jdbcType=VARCHAR}");
        }
        
        if (record.getImgaddress() != null) {
            SET("imgAddress = #{imgaddress,jdbcType=VARCHAR}");
        }
        
        if (record.getStat() != null) {
            SET("stat = #{stat,jdbcType=INTEGER}");
        }
        
        if (record.getSequence() != null) {
            SET("sequence = #{sequence,jdbcType=INTEGER}");
        }
        
        WHERE("uuid = #{uuid,jdbcType=VARCHAR}");
        
        return SQL();
    }

    protected void applyWhere(SwipersExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}
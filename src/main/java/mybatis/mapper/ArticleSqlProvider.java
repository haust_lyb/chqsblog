package mybatis.mapper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import java.util.List;
import java.util.Map;
import mybatis.model.Article;
import mybatis.model.ArticleExample.Criteria;
import mybatis.model.ArticleExample.Criterion;
import mybatis.model.ArticleExample;

public class ArticleSqlProvider {

    public String countByExample(ArticleExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("article");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(ArticleExample example) {
        BEGIN();
        DELETE_FROM("article");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(Article record) {
        BEGIN();
        INSERT_INTO("article");
        
        if (record.getUuid() != null) {
            VALUES("uuid", "#{uuid,jdbcType=VARCHAR}");
        }
        
        if (record.getIsprivate() != null) {
            VALUES("isPrivate", "#{isprivate,jdbcType=INTEGER}");
        }
        
        if (record.getCreatetime() != null) {
            VALUES("createTime", "#{createtime,jdbcType=BIGINT}");
        }
        
        if (record.getLastedittime() != null) {
            VALUES("lastEditTime", "#{lastedittime,jdbcType=BIGINT}");
        }
        
        if (record.getAuthoruuid() != null) {
            VALUES("authorUuid", "#{authoruuid,jdbcType=VARCHAR}");
        }
        
        if (record.getTypeuuid() != null) {
            VALUES("typeUuid", "#{typeuuid,jdbcType=VARCHAR}");
        }
        
        if (record.getDescription() != null) {
            VALUES("description", "#{description,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            VALUES("title", "#{title,jdbcType=VARCHAR}");
        }
        
        if (record.getContent() != null) {
            VALUES("content", "#{content,jdbcType=LONGVARCHAR}");
        }
        
        return SQL();
    }

    public String selectByExampleWithBLOBs(ArticleExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("uuid");
        } else {
            SELECT("uuid");
        }
        SELECT("isPrivate");
        SELECT("createTime");
        SELECT("lastEditTime");
        SELECT("authorUuid");
        SELECT("typeUuid");
        SELECT("description");
        SELECT("title");
        SELECT("content");
        FROM("article");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String selectByExample(ArticleExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("uuid");
        } else {
            SELECT("uuid");
        }
        SELECT("isPrivate");
        SELECT("createTime");
        SELECT("lastEditTime");
        SELECT("authorUuid");
        SELECT("typeUuid");
        SELECT("description");
        SELECT("title");
        FROM("article");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        Article record = (Article) parameter.get("record");
        ArticleExample example = (ArticleExample) parameter.get("example");
        
        BEGIN();
        UPDATE("article");
        
        if (record.getUuid() != null) {
            SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        }
        
        if (record.getIsprivate() != null) {
            SET("isPrivate = #{record.isprivate,jdbcType=INTEGER}");
        }
        
        if (record.getCreatetime() != null) {
            SET("createTime = #{record.createtime,jdbcType=BIGINT}");
        }
        
        if (record.getLastedittime() != null) {
            SET("lastEditTime = #{record.lastedittime,jdbcType=BIGINT}");
        }
        
        if (record.getAuthoruuid() != null) {
            SET("authorUuid = #{record.authoruuid,jdbcType=VARCHAR}");
        }
        
        if (record.getTypeuuid() != null) {
            SET("typeUuid = #{record.typeuuid,jdbcType=VARCHAR}");
        }
        
        if (record.getDescription() != null) {
            SET("description = #{record.description,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{record.title,jdbcType=VARCHAR}");
        }
        
        if (record.getContent() != null) {
            SET("content = #{record.content,jdbcType=LONGVARCHAR}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExampleWithBLOBs(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("article");
        
        SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        SET("isPrivate = #{record.isprivate,jdbcType=INTEGER}");
        SET("createTime = #{record.createtime,jdbcType=BIGINT}");
        SET("lastEditTime = #{record.lastedittime,jdbcType=BIGINT}");
        SET("authorUuid = #{record.authoruuid,jdbcType=VARCHAR}");
        SET("typeUuid = #{record.typeuuid,jdbcType=VARCHAR}");
        SET("description = #{record.description,jdbcType=VARCHAR}");
        SET("title = #{record.title,jdbcType=VARCHAR}");
        SET("content = #{record.content,jdbcType=LONGVARCHAR}");
        
        ArticleExample example = (ArticleExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("article");
        
        SET("uuid = #{record.uuid,jdbcType=VARCHAR}");
        SET("isPrivate = #{record.isprivate,jdbcType=INTEGER}");
        SET("createTime = #{record.createtime,jdbcType=BIGINT}");
        SET("lastEditTime = #{record.lastedittime,jdbcType=BIGINT}");
        SET("authorUuid = #{record.authoruuid,jdbcType=VARCHAR}");
        SET("typeUuid = #{record.typeuuid,jdbcType=VARCHAR}");
        SET("description = #{record.description,jdbcType=VARCHAR}");
        SET("title = #{record.title,jdbcType=VARCHAR}");
        
        ArticleExample example = (ArticleExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(Article record) {
        BEGIN();
        UPDATE("article");
        
        if (record.getIsprivate() != null) {
            SET("isPrivate = #{isprivate,jdbcType=INTEGER}");
        }
        
        if (record.getCreatetime() != null) {
            SET("createTime = #{createtime,jdbcType=BIGINT}");
        }
        
        if (record.getLastedittime() != null) {
            SET("lastEditTime = #{lastedittime,jdbcType=BIGINT}");
        }
        
        if (record.getAuthoruuid() != null) {
            SET("authorUuid = #{authoruuid,jdbcType=VARCHAR}");
        }
        
        if (record.getTypeuuid() != null) {
            SET("typeUuid = #{typeuuid,jdbcType=VARCHAR}");
        }
        
        if (record.getDescription() != null) {
            SET("description = #{description,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{title,jdbcType=VARCHAR}");
        }
        
        if (record.getContent() != null) {
            SET("content = #{content,jdbcType=LONGVARCHAR}");
        }
        
        WHERE("uuid = #{uuid,jdbcType=VARCHAR}");
        
        return SQL();
    }

    protected void applyWhere(ArticleExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}
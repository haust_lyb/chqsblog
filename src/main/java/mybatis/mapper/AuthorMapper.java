package mybatis.mapper;

import java.util.List;
import mybatis.model.Author;
import mybatis.model.AuthorExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorMapper {
    @SelectProvider(type=AuthorSqlProvider.class, method="countByExample")
    int countByExample(AuthorExample example);

    @DeleteProvider(type=AuthorSqlProvider.class, method="deleteByExample")
    int deleteByExample(AuthorExample example);

    @Delete({
        "delete from author",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into author (uuid, loginPass, ",
        "loginName, stat, ",
        "levelUuid, nickName, ",
        "sex, headImg, phone, ",
        "age, address)",
        "values (#{uuid,jdbcType=VARCHAR}, #{loginpass,jdbcType=VARCHAR}, ",
        "#{loginname,jdbcType=VARCHAR}, #{stat,jdbcType=INTEGER}, ",
        "#{leveluuid,jdbcType=VARCHAR}, #{nickname,jdbcType=VARCHAR}, ",
        "#{sex,jdbcType=TINYINT}, #{headimg,jdbcType=VARCHAR}, #{phone,jdbcType=VARCHAR}, ",
        "#{age,jdbcType=INTEGER}, #{address,jdbcType=VARCHAR})"
    })
    int insert(Author record);

    @InsertProvider(type=AuthorSqlProvider.class, method="insertSelective")
    int insertSelective(Author record);

    @SelectProvider(type=AuthorSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="loginPass", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="loginName", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="stat", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="levelUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="nickName", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="sex", javaType=Byte.class, jdbcType=JdbcType.TINYINT),
        @Arg(column="headImg", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="phone", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="age", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="address", javaType=String.class, jdbcType=JdbcType.VARCHAR)
    })
    List<Author> selectByExample(AuthorExample example);

    @Select({
        "select",
        "uuid, loginPass, loginName, stat, levelUuid, nickName, sex, headImg, phone, ",
        "age, address",
        "from author",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="loginPass", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="loginName", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="stat", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="levelUuid", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="nickName", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="sex", javaType=Byte.class, jdbcType=JdbcType.TINYINT),
        @Arg(column="headImg", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="phone", javaType=String.class, jdbcType=JdbcType.VARCHAR),
        @Arg(column="age", javaType=Integer.class, jdbcType=JdbcType.INTEGER),
        @Arg(column="address", javaType=String.class, jdbcType=JdbcType.VARCHAR)
    })
    Author selectByPrimaryKey(String uuid);

    @UpdateProvider(type=AuthorSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Author record, @Param("example") AuthorExample example);

    @UpdateProvider(type=AuthorSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Author record, @Param("example") AuthorExample example);

    @UpdateProvider(type=AuthorSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Author record);

    @Update({
        "update author",
        "set loginPass = #{loginpass,jdbcType=VARCHAR},",
          "loginName = #{loginname,jdbcType=VARCHAR},",
          "stat = #{stat,jdbcType=INTEGER},",
          "levelUuid = #{leveluuid,jdbcType=VARCHAR},",
          "nickName = #{nickname,jdbcType=VARCHAR},",
          "sex = #{sex,jdbcType=TINYINT},",
          "headImg = #{headimg,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=VARCHAR},",
          "age = #{age,jdbcType=INTEGER},",
          "address = #{address,jdbcType=VARCHAR}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Author record);
}
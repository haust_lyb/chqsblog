package mybatis.mapper;

import java.util.List;
import mybatis.model.Level;
import mybatis.model.LevelExample;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

@Repository
public interface LevelMapper {
    @SelectProvider(type=LevelSqlProvider.class, method="countByExample")
    int countByExample(LevelExample example);

    @DeleteProvider(type=LevelSqlProvider.class, method="deleteByExample")
    int deleteByExample(LevelExample example);

    @Delete({
        "delete from level",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String uuid);

    @Insert({
        "insert into level (uuid, levelNum)",
        "values (#{uuid,jdbcType=VARCHAR}, #{levelnum,jdbcType=INTEGER})"
    })
    int insert(Level record);

    @InsertProvider(type=LevelSqlProvider.class, method="insertSelective")
    int insertSelective(Level record);

    @SelectProvider(type=LevelSqlProvider.class, method="selectByExample")
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="levelNum", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    List<Level> selectByExample(LevelExample example);

    @Select({
        "select",
        "uuid, levelNum",
        "from level",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    @ConstructorArgs({
        @Arg(column="uuid", javaType=String.class, jdbcType=JdbcType.VARCHAR, id=true),
        @Arg(column="levelNum", javaType=Integer.class, jdbcType=JdbcType.INTEGER)
    })
    Level selectByPrimaryKey(String uuid);

    @UpdateProvider(type=LevelSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Level record, @Param("example") LevelExample example);

    @UpdateProvider(type=LevelSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Level record, @Param("example") LevelExample example);

    @UpdateProvider(type=LevelSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Level record);

    @Update({
        "update level",
        "set levelNum = #{levelnum,jdbcType=INTEGER}",
        "where uuid = #{uuid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(Level record);
}
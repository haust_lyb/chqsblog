package mybatis.model;

import java.util.ArrayList;
import java.util.List;

public class CommentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CommentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andFromauthorIsNull() {
            addCriterion("fromAuthor is null");
            return (Criteria) this;
        }

        public Criteria andFromauthorIsNotNull() {
            addCriterion("fromAuthor is not null");
            return (Criteria) this;
        }

        public Criteria andFromauthorEqualTo(String value) {
            addCriterion("fromAuthor =", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorNotEqualTo(String value) {
            addCriterion("fromAuthor <>", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorGreaterThan(String value) {
            addCriterion("fromAuthor >", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorGreaterThanOrEqualTo(String value) {
            addCriterion("fromAuthor >=", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorLessThan(String value) {
            addCriterion("fromAuthor <", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorLessThanOrEqualTo(String value) {
            addCriterion("fromAuthor <=", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorLike(String value) {
            addCriterion("fromAuthor like", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorNotLike(String value) {
            addCriterion("fromAuthor not like", value, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorIn(List<String> values) {
            addCriterion("fromAuthor in", values, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorNotIn(List<String> values) {
            addCriterion("fromAuthor not in", values, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorBetween(String value1, String value2) {
            addCriterion("fromAuthor between", value1, value2, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andFromauthorNotBetween(String value1, String value2) {
            addCriterion("fromAuthor not between", value1, value2, "fromauthor");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Long value) {
            addCriterion("createTime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Long value) {
            addCriterion("createTime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Long value) {
            addCriterion("createTime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Long value) {
            addCriterion("createTime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Long value) {
            addCriterion("createTime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Long value) {
            addCriterion("createTime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Long> values) {
            addCriterion("createTime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Long> values) {
            addCriterion("createTime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Long value1, Long value2) {
            addCriterion("createTime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Long value1, Long value2) {
            addCriterion("createTime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andArticleuuidIsNull() {
            addCriterion("articleUuid is null");
            return (Criteria) this;
        }

        public Criteria andArticleuuidIsNotNull() {
            addCriterion("articleUuid is not null");
            return (Criteria) this;
        }

        public Criteria andArticleuuidEqualTo(String value) {
            addCriterion("articleUuid =", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidNotEqualTo(String value) {
            addCriterion("articleUuid <>", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidGreaterThan(String value) {
            addCriterion("articleUuid >", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidGreaterThanOrEqualTo(String value) {
            addCriterion("articleUuid >=", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidLessThan(String value) {
            addCriterion("articleUuid <", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidLessThanOrEqualTo(String value) {
            addCriterion("articleUuid <=", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidLike(String value) {
            addCriterion("articleUuid like", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidNotLike(String value) {
            addCriterion("articleUuid not like", value, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidIn(List<String> values) {
            addCriterion("articleUuid in", values, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidNotIn(List<String> values) {
            addCriterion("articleUuid not in", values, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidBetween(String value1, String value2) {
            addCriterion("articleUuid between", value1, value2, "articleuuid");
            return (Criteria) this;
        }

        public Criteria andArticleuuidNotBetween(String value1, String value2) {
            addCriterion("articleUuid not between", value1, value2, "articleuuid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
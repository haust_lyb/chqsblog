package mybatis.model;

public class Swipers {
    private String uuid;

    private String href;

    private String title;

    private String imgaddress;

    private Integer stat;

    private Integer sequence;

    public Swipers(String uuid, String href, String title, String imgaddress, Integer stat, Integer sequence) {
        this.uuid = uuid;
        this.href = href;
        this.title = title;
        this.imgaddress = imgaddress;
        this.stat = stat;
        this.sequence = sequence;
    }

    public Swipers() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgaddress() {
        return imgaddress;
    }

    public void setImgaddress(String imgaddress) {
        this.imgaddress = imgaddress;
    }

    public Integer getStat() {
        return stat;
    }

    public void setStat(Integer stat) {
        this.stat = stat;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
}
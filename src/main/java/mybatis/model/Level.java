package mybatis.model;

public class Level {
    private String uuid;

    private Integer levelnum;

    public Level(String uuid, Integer levelnum) {
        this.uuid = uuid;
        this.levelnum = levelnum;
    }

    public Level() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getLevelnum() {
        return levelnum;
    }

    public void setLevelnum(Integer levelnum) {
        this.levelnum = levelnum;
    }
}
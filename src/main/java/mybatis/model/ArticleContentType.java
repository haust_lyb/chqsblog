package mybatis.model;

public class ArticleContentType {
    private String uuid;

    private String typename;

    public ArticleContentType(String uuid, String typename) {
        this.uuid = uuid;
        this.typename = typename;
    }

    public ArticleContentType() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
}
package mybatis.model;

public class Admin {
    private String uuid;

    private String loginname;

    private String loginpass;

    private Integer type;

    public Admin(String uuid, String loginname, String loginpass, Integer type) {
        this.uuid = uuid;
        this.loginname = loginname;
        this.loginpass = loginpass;
        this.type = type;
    }

    public Admin() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getLoginpass() {
        return loginpass;
    }

    public void setLoginpass(String loginpass) {
        this.loginpass = loginpass;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
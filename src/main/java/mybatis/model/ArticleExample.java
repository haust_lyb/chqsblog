package mybatis.model;

import java.util.ArrayList;
import java.util.List;

public class ArticleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ArticleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andIsprivateIsNull() {
            addCriterion("isPrivate is null");
            return (Criteria) this;
        }

        public Criteria andIsprivateIsNotNull() {
            addCriterion("isPrivate is not null");
            return (Criteria) this;
        }

        public Criteria andIsprivateEqualTo(Integer value) {
            addCriterion("isPrivate =", value, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateNotEqualTo(Integer value) {
            addCriterion("isPrivate <>", value, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateGreaterThan(Integer value) {
            addCriterion("isPrivate >", value, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateGreaterThanOrEqualTo(Integer value) {
            addCriterion("isPrivate >=", value, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateLessThan(Integer value) {
            addCriterion("isPrivate <", value, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateLessThanOrEqualTo(Integer value) {
            addCriterion("isPrivate <=", value, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateIn(List<Integer> values) {
            addCriterion("isPrivate in", values, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateNotIn(List<Integer> values) {
            addCriterion("isPrivate not in", values, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateBetween(Integer value1, Integer value2) {
            addCriterion("isPrivate between", value1, value2, "isprivate");
            return (Criteria) this;
        }

        public Criteria andIsprivateNotBetween(Integer value1, Integer value2) {
            addCriterion("isPrivate not between", value1, value2, "isprivate");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Long value) {
            addCriterion("createTime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Long value) {
            addCriterion("createTime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Long value) {
            addCriterion("createTime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Long value) {
            addCriterion("createTime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Long value) {
            addCriterion("createTime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Long value) {
            addCriterion("createTime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Long> values) {
            addCriterion("createTime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Long> values) {
            addCriterion("createTime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Long value1, Long value2) {
            addCriterion("createTime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Long value1, Long value2) {
            addCriterion("createTime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeIsNull() {
            addCriterion("lastEditTime is null");
            return (Criteria) this;
        }

        public Criteria andLastedittimeIsNotNull() {
            addCriterion("lastEditTime is not null");
            return (Criteria) this;
        }

        public Criteria andLastedittimeEqualTo(Long value) {
            addCriterion("lastEditTime =", value, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeNotEqualTo(Long value) {
            addCriterion("lastEditTime <>", value, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeGreaterThan(Long value) {
            addCriterion("lastEditTime >", value, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeGreaterThanOrEqualTo(Long value) {
            addCriterion("lastEditTime >=", value, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeLessThan(Long value) {
            addCriterion("lastEditTime <", value, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeLessThanOrEqualTo(Long value) {
            addCriterion("lastEditTime <=", value, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeIn(List<Long> values) {
            addCriterion("lastEditTime in", values, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeNotIn(List<Long> values) {
            addCriterion("lastEditTime not in", values, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeBetween(Long value1, Long value2) {
            addCriterion("lastEditTime between", value1, value2, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andLastedittimeNotBetween(Long value1, Long value2) {
            addCriterion("lastEditTime not between", value1, value2, "lastedittime");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidIsNull() {
            addCriterion("authorUuid is null");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidIsNotNull() {
            addCriterion("authorUuid is not null");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidEqualTo(String value) {
            addCriterion("authorUuid =", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidNotEqualTo(String value) {
            addCriterion("authorUuid <>", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidGreaterThan(String value) {
            addCriterion("authorUuid >", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidGreaterThanOrEqualTo(String value) {
            addCriterion("authorUuid >=", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidLessThan(String value) {
            addCriterion("authorUuid <", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidLessThanOrEqualTo(String value) {
            addCriterion("authorUuid <=", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidLike(String value) {
            addCriterion("authorUuid like", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidNotLike(String value) {
            addCriterion("authorUuid not like", value, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidIn(List<String> values) {
            addCriterion("authorUuid in", values, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidNotIn(List<String> values) {
            addCriterion("authorUuid not in", values, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidBetween(String value1, String value2) {
            addCriterion("authorUuid between", value1, value2, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andAuthoruuidNotBetween(String value1, String value2) {
            addCriterion("authorUuid not between", value1, value2, "authoruuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidIsNull() {
            addCriterion("typeUuid is null");
            return (Criteria) this;
        }

        public Criteria andTypeuuidIsNotNull() {
            addCriterion("typeUuid is not null");
            return (Criteria) this;
        }

        public Criteria andTypeuuidEqualTo(String value) {
            addCriterion("typeUuid =", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidNotEqualTo(String value) {
            addCriterion("typeUuid <>", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidGreaterThan(String value) {
            addCriterion("typeUuid >", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidGreaterThanOrEqualTo(String value) {
            addCriterion("typeUuid >=", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidLessThan(String value) {
            addCriterion("typeUuid <", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidLessThanOrEqualTo(String value) {
            addCriterion("typeUuid <=", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidLike(String value) {
            addCriterion("typeUuid like", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidNotLike(String value) {
            addCriterion("typeUuid not like", value, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidIn(List<String> values) {
            addCriterion("typeUuid in", values, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidNotIn(List<String> values) {
            addCriterion("typeUuid not in", values, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidBetween(String value1, String value2) {
            addCriterion("typeUuid between", value1, value2, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andTypeuuidNotBetween(String value1, String value2) {
            addCriterion("typeUuid not between", value1, value2, "typeuuid");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
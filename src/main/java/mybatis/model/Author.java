package mybatis.model;

public class Author {
    private String uuid;

    private String loginpass;

    private String loginname;

    private Integer stat;

    private String leveluuid;

    private String nickname;

    private Byte sex;

    private String headimg;

    private String phone;

    private Integer age;

    private String address;

    public Author(String uuid, String loginpass, String loginname, Integer stat, String leveluuid, String nickname, Byte sex, String headimg, String phone, Integer age, String address) {
        this.uuid = uuid;
        this.loginpass = loginpass;
        this.loginname = loginname;
        this.stat = stat;
        this.leveluuid = leveluuid;
        this.nickname = nickname;
        this.sex = sex;
        this.headimg = headimg;
        this.phone = phone;
        this.age = age;
        this.address = address;
    }

    public Author() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLoginpass() {
        return loginpass;
    }

    public void setLoginpass(String loginpass) {
        this.loginpass = loginpass;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public Integer getStat() {
        return stat;
    }

    public void setStat(Integer stat) {
        this.stat = stat;
    }

    public String getLeveluuid() {
        return leveluuid;
    }

    public void setLeveluuid(String leveluuid) {
        this.leveluuid = leveluuid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
package mybatis.model;

public class FriendLink {
    private String uuid;

    private String href;

    private String name;

    private Integer stat;

    public FriendLink(String uuid, String href, String name, Integer stat) {
        this.uuid = uuid;
        this.href = href;
        this.name = name;
        this.stat = stat;
    }

    public FriendLink() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStat() {
        return stat;
    }

    public void setStat(Integer stat) {
        this.stat = stat;
    }
}
package mybatis.model;

public class Comment {
    private String uuid;

    private String fromauthor;

    private Long createtime;

    private String articleuuid;

    private String content;

    public Comment(String uuid, String fromauthor, Long createtime, String articleuuid, String content) {
        this.uuid = uuid;
        this.fromauthor = fromauthor;
        this.createtime = createtime;
        this.articleuuid = articleuuid;
        this.content = content;
    }

    public Comment() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFromauthor() {
        return fromauthor;
    }

    public void setFromauthor(String fromauthor) {
        this.fromauthor = fromauthor;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getArticleuuid() {
        return articleuuid;
    }

    public void setArticleuuid(String articleuuid) {
        this.articleuuid = articleuuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
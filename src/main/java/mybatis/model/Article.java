package mybatis.model;

public class Article {
    private String uuid;

    private Integer isprivate;

    private Long createtime;

    private Long lastedittime;

    private String authoruuid;

    private String typeuuid;

    private String description;

    private String title;

    private String content;

    public Article(String uuid, Integer isprivate, Long createtime, Long lastedittime, String authoruuid, String typeuuid, String description, String title, String content) {
        this.uuid = uuid;
        this.isprivate = isprivate;
        this.createtime = createtime;
        this.lastedittime = lastedittime;
        this.authoruuid = authoruuid;
        this.typeuuid = typeuuid;
        this.description = description;
        this.title = title;
        this.content = content;
    }

    public Article() {
        super();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getIsprivate() {
        return isprivate;
    }

    public void setIsprivate(Integer isprivate) {
        this.isprivate = isprivate;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getLastedittime() {
        return lastedittime;
    }

    public void setLastedittime(Long lastedittime) {
        this.lastedittime = lastedittime;
    }

    public String getAuthoruuid() {
        return authoruuid;
    }

    public void setAuthoruuid(String authoruuid) {
        this.authoruuid = authoruuid;
    }

    public String getTypeuuid() {
        return typeuuid;
    }

    public void setTypeuuid(String typeuuid) {
        this.typeuuid = typeuuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
package mybatis.dto;

import com.lyb.utils.MyUtil;

import java.util.HashMap;
import java.util.UUID;

/**
 * 邮件的key和value
 */
public class MailMap {

    private static HashMap<String,String> mailListMap=new HashMap<String, String>();

    public static String addMailAddressToMap(String email){
        String key= MyUtil.getUuid();
        mailListMap.put(key,email);
        //key是UUID value是address
        return key;
    }


    public static String getMailByKey(String key){
        return mailListMap.remove(key);
    }
}

package service.customerServiceImpl;

import mybatis.mapper.AuthorMapper;
import mybatis.mapper.LevelMapper;
import mybatis.model.Author;
import mybatis.model.AuthorExample;
import mybatis.model.Level;
import mybatis.model.LevelExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.BaseService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Liyibo on 2018/4/10.
 */
@Service
public class AuthorServiceImpl extends BaseService{
    @Autowired
    AuthorMapper authorMapper;

    @Autowired
    LevelMapper levelMapper;

    public Map<Author,Integer> getAuthorList(){
        AuthorExample authorExample=new AuthorExample();
        AuthorExample.Criteria criteria=authorExample.createCriteria();
        criteria.andUuidIsNotNull();
        ArrayList<Author> authorlist = (ArrayList<Author>) authorMapper.selectByExample(authorExample);



        Map<Author,Integer> map=new HashMap<Author, Integer>();

        for (Author author:authorlist) {
            Level level=levelMapper.selectByPrimaryKey(author.getLeveluuid());
           map.put(author,level.getLevelnum());
        }

//
//        ArrayList<Map<Author,Level>> list=new ArrayList<Map<Author, Level>>();
//
        return map;
    }

    public Author getAuthorByUuid(String authoruuid) {
        return authorMapper.selectByPrimaryKey(authoruuid);
    }
}

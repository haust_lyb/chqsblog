package service.managerServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailSenderServiceImpl {
    @Autowired
    private JavaMailSender mailSender;


    @Autowired
    private SpringTemplateEngine thymeleaf;


    public boolean sendMail(String to, String title, String text) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("18437963681@163.com");
        mailMessage.setTo(to);
        mailMessage.setSubject(title);
        mailMessage.setText(text);
        try {
            mailSender.send(mailMessage);
        } catch (Exception e) {
            return false;
        }
//
        return true;

    }


    public boolean sendMimeMessageMail(String to, String title, String text) throws MessagingException {
        //遗留问题 乱码问题

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);//true代表是multipart类型

        helper.setFrom("18437963681@163.com");
        helper.setTo(to);
        helper.setSubject(title);
        helper.setText(text);

        Context ctx = new Context();
        ctx.setVariable("text", text);
        ctx.setVariable("title", title);
        String emailText = thymeleaf.process("sayhello.html", ctx);
        helper.setText(emailText, true);


        //添加附件
//        ClassPathResource image=new ClassPathResource("classpathThymeleafTemplate/icon.png");
//        helper.addInline("chqsbloglogo",image);

        try {
            mailSender.send(mimeMessage);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean sendMimeMessageMail(String to, String title, String text,String key) throws MessagingException {
        //遗留问题 乱码问题

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);//true代表是multipart类型

        helper.setFrom("18437963681@163.com");
        helper.setTo(to);
        helper.setSubject(title);
        helper.setText(text);

        Context ctx = new Context();
        ctx.setVariable("text", text);
        ctx.setVariable("title", title);
        ctx.setVariable("key",key);
        String emailText = thymeleaf.process("sayhello.html", ctx);
        helper.setText(emailText, true);


        //添加附件
//        ClassPathResource image=new ClassPathResource("classpathThymeleafTemplate/icon.png");
//        helper.addInline("chqsbloglogo",image);

        try {
            mailSender.send(mimeMessage);
            return true;
        } catch (Exception e) {
            return false;
        }
    }



}

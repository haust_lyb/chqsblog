package service.managerServiceImpl;

import mybatis.mapper.AdminMapper;
import mybatis.mapper.AdminSqlProvider;
import mybatis.model.Admin;
import mybatis.model.AdminExample;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.BaseService;

import java.util.ArrayList;

@Service
public class AdminServiceImpl extends BaseService{

    @Autowired
    AdminMapper adminMapper;

    public Admin getAdmin(String uuid) {
        Admin admin=adminMapper.selectByPrimaryKey(uuid);
        return admin;
    }


    public Admin check(Admin admin){
        AdminExample adminExample=new AdminExample();
        AdminExample.Criteria criteria=adminExample.createCriteria();
        criteria.andLoginnameEqualTo(admin.getLoginname()).andLoginpassEqualTo(admin.getLoginpass());


        ArrayList<Admin> list=(ArrayList<Admin>) adminMapper.selectByExample(adminExample);
        if (list.size()!=0){
            return list.get(0);
        }
        else {
            return null;
        }

    }


}

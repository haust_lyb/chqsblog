package service.managerServiceImpl;

import mybatis.mapper.AdminMapper;
import mybatis.mapper.ArticleMapper;
import mybatis.model.Admin;
import mybatis.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.BaseService;

@Service
public class ArticleServiceImpl extends BaseService {

    @Autowired
    ArticleMapper articleMapper;

    public int addArticle(Article article) {

        return articleMapper.insert(article);
    }


}

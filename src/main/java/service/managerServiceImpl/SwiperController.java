package service.managerServiceImpl;

import com.lyb.utils.MyUtil;
import mybatis.dto.MsgData;
import mybatis.mapper.SwipersMapper;
import mybatis.model.Swipers;
import mybatis.model.SwipersExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("admin")
public class SwiperController {
    @Autowired
    SwipersMapper swipersMapper;

    @RequestMapping("addSwiper")
    @ResponseBody
    public MsgData addSwiper(Swipers swipers){
        Swipers s=new Swipers();
        s.setUuid(MyUtil.getUuid());
        s.setHref("http//:www.baicu.com");
        s.setImgaddress("ssssssssssssssssssssssss");
        s.setStat(1);
        s.setSequence(1);
        s.setTitle("666,簡直不能在六了");
        int res=swipersMapper.insert(s);
        return new MsgData(0,"成功添加",res);
    }


    @RequestMapping("updataSwiper")
    @ResponseBody
    public MsgData updataSwiper(String uuid){


//        SwipersExample swipersExample=new SwipersExample();
//        SwipersExample.Criteria criteria=swipersExample.createCriteria();
//        criteria.andUuidEqualTo(uuid);
        Swipers s=swipersMapper.selectByPrimaryKey(uuid);
        if (s!=null) {
            s.setStat(1);
            s.setSequence(1);
            s.setTitle("666,簡直不能在6了");
        }

        int res=swipersMapper.updateByPrimaryKey(s);

        if (res!=0){
        return new MsgData(0,"成功修改",res);}
        else {
            return new MsgData(-1,"修改失敗",res);
        }
    }
}

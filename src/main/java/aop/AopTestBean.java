package aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class AopTestBean {
//    @Autowired
//    Logger logger;

    @Pointcut("execution(* service.managerServiceImpl.*.*(..))")
    public void baseaop() {
    }

    @Before("baseaop()")
    public void doSomeThingBefore() {
        // System.out.println(this.getClass().getName()+"SayHello() is running ...");
    }
}

package controller.manager;

import com.lyb.utils.MyUtil;
import controller.BaseController;
import mybatis.dto.MsgData;
import mybatis.mapper.ArticleMapper;
import mybatis.model.Admin;
import mybatis.model.Article;
import mybatis.model.ArticleExample;
import mybatis.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.managerServiceImpl.AdminServiceImpl;
import service.managerServiceImpl.ArticleServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;

@Controller
@RequestMapping("admin")
public class ArticleController extends BaseController{
    @Autowired
    ArticleServiceImpl articleService;

    @Autowired
    ArticleMapper articleMapper;



    @RequestMapping("doAddArticle")
    public String doAddArticle(Article article, HttpServletRequest request) {
        if (!this.AdminLoginCheck(request,null)){
            return "admin/login.jsp";
        }

        article.setUuid(MyUtil.getUuid());
        article.setCreatetime(MyUtil.dateTransfer(new Date()));
        System.out.println(articleService.addArticle(article));
        return "admin/articleListPart.jsp";
    }




    @RequestMapping("addArticle")
    public String addArticle(HttpServletRequest httpServletRequest) {
        if (!this.AdminLoginCheck(httpServletRequest,null)){
            return "admin/login.jsp";
        }
        return "admin/articleAddPart.jsp";
    }


    @RequestMapping("ArticleList")
    @ResponseBody
    public ArrayList<Article> ArticleManager(){
        ArticleExample articleExample=new ArticleExample();
        ArticleExample.Criteria criteria=articleExample.createCriteria();
        criteria.andIsprivateEqualTo(0);
        return (ArrayList<Article>)articleMapper.selectByExampleWithBLOBs(articleExample);
    }


}

package controller.manager;

import controller.BaseController;
import mybatis.dto.MsgData;
import mybatis.mapper.AuthorMapper;
import mybatis.model.*;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.customerServiceImpl.AuthorServiceImpl;
import service.managerServiceImpl.AdminServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Liyibo on 2018/4/10.
 */
@Controller
@RequestMapping("admin")
public class UserController extends BaseController{

    @Autowired
    AuthorServiceImpl authorService;

    @Autowired
    AuthorMapper authorMapper;

    @Autowired
    AdminServiceImpl adminService;


    @ResponseBody//返回为json
    @RequestMapping("getAuthorList")//二级请
    public List<Author> getFiendlyList(){
        AuthorExample authorExample=new AuthorExample();
        AuthorExample.Criteria criteria=authorExample.createCriteria();
        criteria.andUuidIsNotNull();

        return authorMapper.selectByExample(authorExample);
    }

    @RequestMapping("logout")
    public String logout(HttpServletRequest request,HttpServletResponse response) {
        request.getSession().removeAttribute("admin");
        return "admin/login.jsp";
    }

    @RequestMapping("getAuthorByUuid")
    @ResponseBody
    public Author getAuthorByUuid(String authoruuid) {
        return authorService.getAuthorByUuid(authoruuid);
    }



    @RequestMapping("login")
    public String managerHome() {
        return "admin/login.jsp";
    }

    @RequestMapping(value = "check", method = RequestMethod.POST)
    @ResponseBody
    public MsgData managerHome(HttpServletRequest request,HttpServletResponse response, @RequestParam("loginName") String loginName, @RequestParam("loginPass") String loginPass) throws Exception {
        HttpSession session = request.getSession();
        Admin admin = new Admin();
        admin.setLoginname(loginName);
        admin.setLoginpass(loginPass);
        Admin checkedAdmin = adminService.check(admin);
        if (checkedAdmin!=null){
            session.setAttribute("admin",checkedAdmin);
            session.setMaxInactiveInterval(900);

            return new MsgData(1,"登陆成功，正在跳转...",null);
        }else {
            return new MsgData(-1,"登录失败，请检查你的用户名和密码!!!",null);
        }

    }

    @RequestMapping("index")
    public String index(HttpServletRequest httpServletRequest) {
        if (!this.AdminLoginCheck(httpServletRequest,null)){
            return "admin/login.jsp";
        }
        return "admin/index.jsp";
    }



    @RequestMapping("authorList")
    @ResponseBody
    public Map<Author,Integer> getAuthorList(){
        return authorService.getAuthorList();
    }



    @RequestMapping("findAdminByid")
    @ResponseBody
    public Admin getAdmin(String id){
        return adminService.getAdmin(id);
    }

}

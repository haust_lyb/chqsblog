package controller.manager;

import mybatis.dto.MsgData;
import mybatis.mapper.SwipersMapper;
import mybatis.model.Swipers;
import mybatis.model.SwipersExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller//标记类
@RequestMapping("admin")//地址栏一级请求
public class SwipersController {
    @Autowired//它可以对类成员变量、方法及构造函数进行标注，完成自动装配的工作
    SwipersMapper swipersMapper;//相当于dao


    @ResponseBody//返回类型为json
    @RequestMapping("getSwipersList")//二级请求
    public List<Swipers> getSwipersList(){//无条件查询
     //建立查询
        SwipersExample swipersExample=new SwipersExample();
        SwipersExample.Criteria criteria=swipersExample.createCriteria();
        criteria.andHrefIsNotNull();//主键不为空的查询出来

        return swipersMapper.selectByExample(swipersExample);
    }

    @ResponseBody
    @RequestMapping("delSwipers")
    public MsgData delSwipers(@RequestParam("uuid") String uuid ){//根据UUID删除
        int count=swipersMapper.deleteByPrimaryKey(uuid);
        if(count==1){
            return new MsgData(1,"删除成功",count);
        }else{
            return new MsgData(-1,"删除失败",count);
        }
    }
}

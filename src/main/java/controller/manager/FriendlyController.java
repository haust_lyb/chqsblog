package controller.manager;

import com.lyb.utils.MyUtil;
import mybatis.dto.MsgData;
import mybatis.mapper.FriendLinkMapper;
import mybatis.model.FriendLink;
import mybatis.model.FriendLinkExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
@RequestMapping("admin")//一级请求
public class FriendlyController {

    @Autowired
    FriendLinkMapper mapper;

    @ResponseBody//返回为json
    @RequestMapping("getFriendlyList")//二级请
    public List<FriendLink> getFiendlyList(){
        FriendLinkExample friendLinkExample=new FriendLinkExample();
        FriendLinkExample.Criteria criteria=friendLinkExample.createCriteria();
        criteria.andUuidIsNotNull();

        return mapper.selectByExample(friendLinkExample);
    }

    @ResponseBody
    @RequestMapping("addFriendLink")
    public MsgData addFriendLink(String href,String name){

        FriendLink friendLink=new FriendLink();
        friendLink.setName(name);
        friendLink.setHref(href);
        friendLink.setStat(1);
        friendLink.setUuid(MyUtil.getUuid());
        int num=mapper.insert(friendLink);
        MsgData msgdata = new MsgData(1,"添加友情链接成功",friendLink);
        return msgdata;
    }

    @ResponseBody//返回为json
    @RequestMapping("delFridenlyLink")//二级请
    public MsgData delFriendlyLink(@RequestParam("uuid") String uuid){
        int num=mapper.deleteByPrimaryKey(uuid);
        if (num==0){
            return new MsgData(-1,"删除失败",num);
        }else {
        return new MsgData(1,"删除成功",num);
        }
    }

    @RequestMapping("editFriendLink")
    public String editFriendLink(@RequestParam("uuid") String uuid, Model model){
        FriendLink friendLink=mapper.selectByPrimaryKey(uuid);
        model.addAttribute("friendLink",friendLink);
        return "admin/editFriendLink.jsp";
    }



    @ResponseBody//返回为json
    @RequestMapping("doEditFriendLink")//二级请
    public MsgData doEditFriendLink(FriendLink friendLink){
        System.out.println(friendLink);
        int num=mapper.updateByPrimaryKey(friendLink);
        if (num==0){
            return new MsgData(-1,"修改失败",num);
        }else {
            return new MsgData(1,"修改成功",num);
        }
    }


}

package controller.manager;

import controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin")
public class MenuListController extends BaseController{

    @RequestMapping(value = "{page}")
    public String returnview(@PathVariable String page){
        return "admin/"+page+".jsp";
    }


}

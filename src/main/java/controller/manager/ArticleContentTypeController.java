package controller.manager;

import mybatis.dto.MsgData;
import mybatis.mapper.ArticleContentTypeMapper;
import mybatis.model.ArticleContentType;
import mybatis.model.ArticleContentTypeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller//标记类
@RequestMapping("admin")//一级
public class ArticleContentTypeController {
    @Autowired//构造方法
     ArticleContentTypeMapper mapper;

    @ResponseBody//json
    @RequestMapping("getArticleContentTypeList")//二级
    public List<ArticleContentType> getArticleContentTypeList(){//无条件查询
        ArticleContentTypeExample example=new ArticleContentTypeExample();
        ArticleContentTypeExample.Criteria criteria=example.createCriteria();
        criteria.andTypenameIsNotNull();//无条件查询

        return mapper.selectByExample(example);
    }

    @ResponseBody
    @RequestMapping("delArticleContentType")
    public MsgData delArticleContentType(@RequestParam("uuid") String uuid){//根据条件删除
        int count=mapper.deleteByPrimaryKey(uuid);
        if(count==1){
            return new MsgData(1,"删除成功",count);
        }else{
            return new MsgData(-1,"删除失败",count);
        }
    }
}

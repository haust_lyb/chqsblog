package controller;

import mybatis.dto.MsgData;
import org.springframework.web.bind.annotation.RequestParam;
import service.managerServiceImpl.MailSenderServiceImpl;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by haiseer on 2018/3/26.
 */
public class BaseController {//所有的controller都要继承这个controller

    protected boolean frontLoginCheck(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        if (httpServletRequest.getSession().getAttribute("author")==null){
            return false;
        }

        return true;


    }


    protected boolean AdminLoginCheck(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        if (httpServletRequest.getSession().getAttribute("admin")==null){
            return false;
        }

        return true;


    }



    public MsgData doRegister(MailSenderServiceImpl mailSenderService,String to, String title, String text, String key) throws MessagingException {
        //http://localhost:8080/front/sendMimeMail?to=1570194845@qq.com&title=mytitle&text=mycontent


        boolean flag = mailSenderService.sendMimeMessageMail(to, title, text,key);

        if (flag){
            return new MsgData(1,"邮件发送成功，请注意查收！",null);
        }
        return new MsgData(1,"邮件发送失败，暂时无法完成注册功能！",null);
    }


}

package controller.customer;

import com.lyb.utils.MyUtil;
import com.sun.xml.internal.rngom.parse.host.Base;
import controller.BaseController;
import mybatis.dto.MailMap;
import mybatis.dto.MsgData;
import mybatis.mapper.AuthorMapper;
import mybatis.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.managerServiceImpl.MailSenderServiceImpl;

import javax.mail.MessagingException;

@Controller
@RequestMapping("mailSender")
public class MailController extends BaseController{

    @Autowired
    AuthorMapper authorMapper;

    @Autowired
    MailSenderServiceImpl mailSenderService;


    //发送普通邮件
    @RequestMapping(value = "sendTextMail", method = RequestMethod.GET)
    @ResponseBody
    public String sendMail(@RequestParam("to") String to, String title, String text) {
        return "" + mailSenderService.sendMail(to, title, text);

    }


    /**
     * 根据前台传来的email地址 发送邮件 并返回发送状态信息
     * @param to
     * @return
     * @throws MessagingException
     */
    @RequestMapping(value = "doRegister", method = RequestMethod.POST)
    @ResponseBody
    public MsgData doRegister(@RequestParam("to") String to) throws MessagingException {

        String key=MailMap.addMailAddressToMap(to);
        key="127.0.0.1:8080/mailSender/authorRegister?key="+key;
        //http://localhost:8080/front/sendMimeMail?to=1570194845@qq.com&title=mytitle&text=mycontent
        return  super.doRegister(mailSenderService,to,"active you account","",key);
    }

    /**
     * 点击了邮件中的超链接
     * @param key
     * @param model
     * @return
     * @throws MessagingException
     */
    @RequestMapping(value = "authorRegister", method = RequestMethod.GET)
    public String authorRegister(@RequestParam("key") String key, Model model) throws MessagingException {
        model.addAttribute("key",key);
        return "front/authorRegister.jsp";//跳转到密码重置页面设置密码
    }


    /**
     * 保存用户密码以及用户合法性教研 看看是否拥有正确的key
     * @param key
     * @param passWord
     * @return
     * @throws MessagingException
     */
    @RequestMapping(value = "checkRegister", method = RequestMethod.POST)
    @ResponseBody
    public MsgData checkRegister(String key,String passWord) throws MessagingException {
        System.out.println(key+"|"+passWord+"|");
        String mailAddress=MailMap.getMailByKey(key);

        if (mailAddress==null){
            return new MsgData(-1,"由于某种因素，导致tocken不可用，请重新发送邮件",null);
        }

        Author author=new Author();
        author.setNickname(mailAddress);
        author.setUuid(MyUtil.getUuid());
        author.setLoginname(mailAddress);
        author.setLoginpass(passWord);
        System.out.println("该用户注册成功\n"+author.toString());
        authorMapper.insert(author);

        return new MsgData(1,"亲！您的账号已经生效！可以在网页上登录了❤",null);
    }


}

package controller.customer;

import com.lyb.utils.MyUtil;
import controller.BaseController;
import mybatis.dto.MsgData;
import mybatis.mapper.ArticleMapper;
import mybatis.mapper.CommentMapper;
import mybatis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.managerServiceImpl.ArticleServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

@Controller
@RequestMapping("front")
public class FrontCommentController extends BaseController {
    @Autowired
    CommentMapper commentMapper;




    @RequestMapping("addComment")
    @ResponseBody
    public MsgData getCommentBuArticleUuid(String articleuuid,String commentcomtext,HttpServletRequest request){
        String authoruuid = ((Author) (request.getSession().getAttribute("author"))).getUuid();
        Comment comment=new Comment();
        comment.setUuid(MyUtil.getUuid());
        comment.setArticleuuid(articleuuid);
        comment.setContent(commentcomtext);
        comment.setFromauthor(authoruuid);
        comment.setCreatetime(new Date().getTime());
        int num=commentMapper.insert(comment);
        if (num>0){
            return new MsgData(1,"添加成功",num);
        }else
        {
            return new MsgData(-1,"添加评论异常",num);
        }

    }




    @ResponseBody
    @RequestMapping("getCommentByArticleUuid")
    public MsgData addComment(String uuid){
        CommentExample commentExamp=new CommentExample();
        CommentExample.Criteria criteria = commentExamp.createCriteria();
        criteria.andArticleuuidEqualTo(uuid);
        ArrayList<Comment> list=(ArrayList<Comment>) commentMapper.selectByExampleWithBLOBs(commentExamp);


        Collections.sort(list, new Comparator<Comment>() {
            public int compare(Comment o1, Comment o2) {
                if (o1.getCreatetime()-o2.getCreatetime()>0){
                    return -1;
                }else {
                    return 1;
                }
            }
        });

        return new MsgData(1,"请求成功",list);
    }




}

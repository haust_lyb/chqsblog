package controller.customer;


import mybatis.dto.MsgData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("test")
public class TestController {



    @RequestMapping("toform1")
    public String totestpostform(){
        return "front/test1.jsp";
    }

    @ResponseBody
    @RequestMapping("form1")
    public MsgData testpostform(String p1,String p2){
        System.out.println(p1+"|"+p2);
        return new MsgData(1,"666",null);
    }

}

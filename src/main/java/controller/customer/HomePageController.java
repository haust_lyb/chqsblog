package controller.customer;

import controller.BaseController;
import controller.ExceptionController;
import mybatis.dto.MsgData;
import mybatis.mapper.AuthorMapper;
import mybatis.mapper.FriendLinkMapper;
import mybatis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.managerServiceImpl.AdminServiceImpl;
import service.managerServiceImpl.MailSenderServiceImpl;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by haiseer on 2018/3/26.
 */
@Controller
@RequestMapping("front")
public class HomePageController extends BaseController {
    @Autowired
    AuthorMapper authorMapper;


    @Autowired
    FriendLinkMapper friendLinkMapper;


    @RequestMapping("home")
    public String getHome(ModelMap model) {
        model.addAttribute("text", "你好");

        //int a=1/0;//不管哪个控制器的哪个方法除了异常我们都可以捕获并统一处理，具体处理方式参考@ControllerAdvice ExceptionController.java
        return "front/home.jsp";//html结尾会去用thymeleaf视图解析器解析 jsp则会由jsp的视图解析器解析
    }



    @RequestMapping("authorLoginCheck")
    @ResponseBody
    public MsgData authorLoginCheck(@RequestParam("loginName") String loginName, @RequestParam("passWord") String passWord, HttpServletRequest request, HttpServletResponse response) {
        Author author=new Author();
        author.setLoginname(loginName);
        author.setLoginpass(passWord);
        AuthorExample authorExample=new AuthorExample();
        AuthorExample.Criteria criteria=authorExample.createCriteria();
        criteria.andLoginnameEqualTo(author.getLoginname());
        criteria.andLoginpassEqualTo(author.getLoginpass());
        ArrayList<Author> list=(ArrayList<Author>) authorMapper.selectByExample(authorExample);
        if(list.size()!=0){
            request.getSession().setAttribute("author",list.get(0));
            return new MsgData(1,"登录成功",null);
        }

        //int a=1/0;//不管哪个控制器的哪个方法除了异常我们都可以捕获并统一处理，具体处理方式参考@ControllerAdvice ExceptionController.java
        return new MsgData(-1,"用户名或密码不正确",null);
    }



    /**
     * 以下 测试相关  暂时没用
     */

    @RequestMapping("login")
    public String login() {
        return "login.jsp";
    }

    @RequestMapping("sitemeshTest")
    public String sitemeshTest() {

        return "front/sitemeshTest.jsp";
    }

    @RequestMapping("sitemeshTest2")
    public String sitemeshTest2() {

        return "home.html";
    }



    @ResponseBody
    @RequestMapping("getAllFriendLink")
    public MsgData getAllFriendLink(){
        FriendLinkExample friendLinkExample=new FriendLinkExample();
        FriendLinkExample.Criteria criteria=friendLinkExample.createCriteria();
        criteria.andStatEqualTo(1);
        ArrayList list=(ArrayList<FriendLink>)friendLinkMapper.selectByExample(friendLinkExample);
        return new MsgData(1,"查询成功",list);
    }
}

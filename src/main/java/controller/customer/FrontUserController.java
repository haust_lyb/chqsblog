package controller.customer;


import mybatis.mapper.AuthorMapper;
import mybatis.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

@Controller
@RequestMapping("front")
public class FrontUserController {

    @Autowired
    AuthorMapper mapper;

    @RequestMapping("userCenter")
    public String userCenter(HttpServletRequest request){
            return "front/userCenter.jsp";
    }

    @RequestMapping(value = "editUserCenter",method = RequestMethod.POST)
    public void editUserCenter(Author author, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        System.out.println(author);
        mapper.updateByPrimaryKeySelective(author);
//        mapper.updateByPrimaryKey(author);
        httpServletRequest.getSession().setAttribute("author",author);
        httpServletResponse.sendRedirect("/front/userCenter");
    }
}

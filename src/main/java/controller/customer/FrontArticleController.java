package controller.customer;

import com.lyb.utils.MyUtil;
import controller.BaseController;
import mybatis.dto.MsgData;
import mybatis.mapper.ArticleMapper;
import mybatis.mapper.ArticleSqlProvider;
import mybatis.model.Article;
import mybatis.model.ArticleExample;
import mybatis.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.managerServiceImpl.ArticleServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

@Controller
@RequestMapping("front")
public class FrontArticleController extends BaseController {
    @Autowired
    ArticleServiceImpl articleService;

    @Autowired
    ArticleMapper articleMapper;


    @RequestMapping(value = "{page}")
    public String toPage(@PathVariable String page) {
        return "front/" + page + ".jsp";
    }

    @RequestMapping("showArticleById")
    public String showArticleInfo(String uuid,Model model) {
        Article article = articleMapper.selectByPrimaryKey(uuid);
        model.addAttribute("articleitem",article);
        return "front/showArticleById.jsp";
    }


    @RequestMapping("doAddArticle")
    public String doAddArticle(Article article, HttpServletRequest request) {
        if (((Author) (request.getSession().getAttribute("author"))) == null) {
            return "front/home.jsp";
        }
        article.setAuthoruuid(((Author) (request.getSession().getAttribute("author"))).getUuid());
        Long time=new Date().getTime();
        article.setUuid(MyUtil.getUuid());
        article.setCreatetime(time);
        article.setLastedittime(time);
        articleService.addArticle(article);


        return "front/addArticleSuccess.jsp";
    }


    @RequestMapping("getMyArticle")
    @ResponseBody
    public MsgData showMyArticleList(HttpServletRequest request) {
        Author author = ((Author) (request.getSession().getAttribute("author")));

        if (author == null) {
            return new MsgData(-1, "失败了,您没有登陆", null);
        } else {
            ArticleExample articleExamp = new ArticleExample();
            ArticleExample.Criteria criteria = articleExamp.createCriteria();
            criteria.andAuthoruuidEqualTo(author.getUuid());


//          criteria.andUuidIsNotNull();


            ArrayList<Article> list = (ArrayList<Article>) articleMapper.selectByExampleWithBLOBs(articleExamp);

            Collections.sort(list, new Comparator<Article>() {//arraylist 根据其中单体对象某字段进行排序
                public int compare(Article o1, Article o2) {
                    if (o1.getCreatetime()-o2.getCreatetime()>0){
                        return -1;
                    }else {
                        return 1;
                    }
                }
            });
            return new MsgData(1, "成功获取data", list);
        }
    }


    @RequestMapping("getAllPublicArticle")
    @ResponseBody
    public MsgData getAllPublicArticle() {

            ArticleExample articleExamp = new ArticleExample();
            ArticleExample.Criteria criteria = articleExamp.createCriteria();
            criteria.andIsprivateEqualTo(0);//公开不加密
            criteria.andUuidIsNotNull();


            ArrayList<Article> list = (ArrayList<Article>) articleMapper.selectByExampleWithBLOBs(articleExamp);

            Collections.sort(list, new Comparator<Article>() {//arraylist 根据其中单体对象某字段进行排序
                public int compare(Article o1, Article o2) {
                    if (o1.getCreatetime()-o2.getCreatetime()>0){
                        return -1;
                    }else {
                        return 1;
                    }
                }
            });
            return new MsgData(1, "成功获取data", list);
    }
}

package filters;

import mybatis.model.Admin;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Liyibo on 2018/4/10.
 */
@WebFilter(filterName = "MyFilter")
public class MyFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
//        System.out.println("filter before...");
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        System.out.println("当前用户正在请求：" + httpServletRequest.getRequestURI());
        chain.doFilter(req,resp);

//        System.out.println("filter after...");
//        HttpServletResponse response = (HttpServletResponse) req;
//        response.addHeader("x-frame-options","SAMEORIGIN");
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

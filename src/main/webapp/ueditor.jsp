<%--
  Created by IntelliJ IDEA.
  User: haiseer
  Date: 2018/4/13
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<script src="/static/plugins/ueditor1.4.3.3/ueditor.config.js"></script>
<script src="/static/plugins/ueditor1.4.3.3/ueditor.all.min.js"></script>
<script src="/static/plugins/ueditor1.4.3.3/lang/zh-cn/zh-cn.js"></script>

<div style="width: 100%;display: flex;">
    <textarea style="flex: 1 1 auto;" id="container" name="content" type="text/plain">欢迎使用UEditor！</textarea>
</div>
<script type="text/javascript">
    var editor = UE.getEditor('container');
</script>
</body>
</html>

<%@ page import="java.util.Date" %>
<%--
  Created by IntelliJ IDEA.
  User: kady
  Date: 17/8/10
  Time: 12:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sm" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<sm:usePage id="frontPage"/>
<!DOCTYPE html>
<html>
<head>
    <!--致谢参考网站 https://www.imsxm.com/
                             ..
                           .' @`._
            ~       ...._.'  ,__.-;
         _..------/`           .-'    ~
        :     __./'       ,  .'-'--.._
     ~   `---(.-'''---.    \`._       `.   ~
       _.--'(  .______.'.-' `-.`        `.
      :      `-..____`-.                  ;
      `.             ````  稻花香里说丰年，  ;   ~
        `-.__           听取人生经验。  __.-'
             ````-----.......-----'''    ~
          ~                   ~
               还请大佬多多指教啦~
   -->
    <meta charset="UTF-8">
    <title>春华秋实博客园</title>
    <!-- 让IE8/9支持媒体查询
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="${pageContext.request.contextPath}/static/plugins/layui/layui.js"></script>

    <!--网站在浏览器中tab页面上的图标-->
    <link rel="icon" href="${pageContext.request.contextPath}/static/source/icon.png" type="image/x-icon"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/myresource/css.css">
    <!-- head 中 -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <script src="${pageContext.request.contextPath}/static/plugins/vue.js"></script>

    <sm:head/>
</head>

<body>

<c:if test="${ frontPage.getProperty('body.template') == null || frontPage.getProperty('body.template').equals('')}">

    <div class="layui-row layui-col-space10">
        <div class="layui-col-md12 ">
                <%--标题咯--%>
            <div style="height: 39px;width: 100%;background: #000;position: fixed;top: 0px;z-index: 500;">
                <div class="layui-container">
                    <div class="layui-row">
                        <a style="color: #fff;line-height: 39px;text-align: center;" href="<c:url value='/front/home'/> "
                           class="layui-col-md1">首页</a>
                        <a style="color: #fff;line-height: 39px;text-align: center;" href="/front/chat"
                           class="layui-col-md1" target="guangchang">广场</a>
                        <a style="color: #fff;line-height: 39px;text-align: center;" href="<c:url value="/front/reward"/>"
                           class="layui-col-md1">请我喝茶</a>

                        <c:if test="${sessionScope.author==null}">
                        <a id="nav-login" style="color: #fff;line-height: 39px;text-align: center;" href="javascript:0"
                           class="layui-col-md1 layui-col-md-offset7">登陆</a>
                        </c:if>
                        <c:if test="${sessionScope.author!=null}">
                            <a id="selfCenter" style="color: #fff;line-height: 39px;text-align: center;" href="<c:url value="/front/userCenter"/>"
                               class="layui-col-md2 layui-col-md-offset6">${sessionScope.author.nickname}</a>
                        </c:if>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <sm:body/>

    <div class="layui-container-fluid" style="margin-top: 10px;">
        <div style="background: #000;height: 60px;display: flex;justify-content: center;align-items: center;align-self: center;">

            <div style="color: #fff;">Design by 李一博【春华秋实博客园】 豫ICP备17048045号</div>

        </div>
    </div>


    <script>
        layui.use(['jquery', 'element', 'carousel', 'code', 'layer', 'form'], function () {
            var form = layui.form;
            layui.code({
                title: "wwww.chqsblog.cn"
            });//可以显示代码了
            var $ = layui.jquery;
            var layer = layui.layer;
            var carousel = layui.carousel;
            var element = layui.element;
            carousel.render({
                elem: '#imgnews',
                width: '100%' //设置容器宽度
                ,
                arrow: 'hover' //始终显示箭头
                //切换动画方式
            });


            $("#nav-login").click(function () {
                layer.open({
                    type: 1,
                    area: ['500px', '300px'],
                    content: $('#login') //这里content是一个DOM，注意：最好该元素要存放在body最外层，否则可能被其它的相对元素所影响
                });
            })


            //监听注册按钮提交
            form.on('submit(formRegister)', function (data) {
                //layer.msg(JSON.stringify(data.field));
                $.ajax({
                    type:'POST',
                    data:data.field,
                    url:"${pageContext.request.contextPath}/mailSender/doRegister",
                    success:function (msg) {
                        if (msg.code!=-1){
                            layer.msg(msg.msg);
                            <%--window.setTimeout(function () {--%>
                            <%--window.location.href="${pageContext.request.contextPath}/admin/index"--%>
                            <%--},1000);--%>
                        }else {
                            layer.msg(msg.msg);
                            $(".layui-input").val("");
                            $(".layui-input:first").focus();
                        }

                    }
                })
                return false;
            });
            //监听登录按钮提交
            form.on('submit(formLogin)', function (data) {
                //layer.msg(JSON.stringify(data.field));
                $.ajax({
                    type:'POST',
                    data:data.field,
                    url:"${pageContext.request.contextPath}/front/authorLoginCheck",
                    success:function (msg) {
                        if (msg.code!=-1){
                            layer.msg(msg.msg);
                            window.setTimeout(function () {
                            window.location.href=window.location.href;
                            },1000);
                        }else {
                            layer.msg(msg.msg);
                            $(".layui-input:first").focus();
                        }

                    }
                })
                return false;
            });
        });

    </script>

    <%--layui用来作为弹出层的元素--%>
    <div style="position: absolute;top:-300px;">
        <div id="login" style="width:500px;">
            <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
                <ul class="layui-tab-title">
                    <li class="layui-this">登陆</li>
                    <li>注册</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                            <%--登陆--%>
                        <form class="layui-form">
                            <div class="layui-form-item">
                                <label class="layui-form-label">邮箱</label>
                                <div class="layui-input-block">
                                    <input type="text" name="loginName" required lay-verify="required" placeholder="请输入邮箱"
                                           autocomplete="off" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label class="layui-form-label">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" name="passWord" required lay-verify="required"
                                           placeholder="请输入密码" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">欢迎登陆</div>
                            </div>

                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formLogin">立即提交</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="layui-tab-item">
                            <%--注册--%>
                        <form class="layui-form">
                            <div class="layui-form-item">
                                <label class="layui-form-label">邮箱</label>
                                <div class="layui-input-block">
                                    <input type="text" name="to" required lay-verify="required" placeholder="请输入邮箱"
                                           autocomplete="off" class="layui-input">
                                </div>
                            </div>



                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formRegister">发送激活邮件</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <%--飘雪元素--%>
    <%--<div id="snow_xb" class="snow_xb">--%>
        <%--<canvas id="Snow"></canvas>--%>
        <%--<script>--%>
            <%--(function () {--%>
                <%--var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||--%>
                    <%--function (callback) {--%>
                        <%--window.setTimeout(callback, 1000 / 60);--%>
                    <%--};--%>
                <%--window.requestAnimationFrame = requestAnimationFrame;--%>
            <%--})();--%>
            <%--(function () {--%>
                <%--var flakes = [],--%>
                    <%--canvas = document.getElementById("Snow"),--%>
                    <%--ctx = canvas.getContext("2d"),--%>
                    <%--flakeCount = 200,--%>
                    <%--mX = -100,--%>
                    <%--mY = -100;--%>
                <%--canvas.width = window.innerWidth;--%>
                <%--canvas.height = window.innerHeight;--%>

                <%--function snow() {--%>
                    <%--ctx.clearRect(0, 0, canvas.width, canvas.height);--%>
                    <%--for (var i = 0; i < flakeCount; i++) {--%>
                        <%--var flake = flakes[i],--%>
                            <%--x = mX,--%>
                            <%--y = mY,--%>
                            <%--minDist = 150,--%>
                            <%--x2 = flake.x,--%>
                            <%--y2 = flake.y;--%>
                        <%--var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),--%>
                            <%--dx = x2 - x,--%>
                            <%--dy = y2 - y;--%>
                        <%--if (dist < minDist) {--%>
                            <%--var force = minDist / (dist * dist),--%>
                                <%--xcomp = (x - x2) / dist,--%>
                                <%--ycomp = (y - y2) / dist,--%>
                                <%--deltaV = force / 2;--%>
                            <%--flake.velX -= deltaV * xcomp;--%>
                            <%--flake.velY -= deltaV * ycomp;--%>
                        <%--} else {--%>
                            <%--flake.velX *= 0.98;--%>
                            <%--if (flake.velY <= flake.speed) {--%>
                                <%--flake.velY = flake.speed;--%>
                            <%--}--%>
                            <%--flake.velX += Math.cos(flake.step += .05) * flake.stepSize;--%>
                        <%--}--%>
                        <%--ctx.fillStyle = "rgba(255,255,255," + flake.opacity + ")";--%>
                        <%--flake.y += flake.velY;--%>
                        <%--flake.x += flake.velX;--%>
                        <%--if (flake.y >= canvas.height || flake.y <= 0) {--%>
                            <%--reset(flake);--%>
                        <%--}--%>
                        <%--if (flake.x >= canvas.width || flake.x <= 0) {--%>
                            <%--reset(flake);--%>
                        <%--}--%>
                        <%--ctx.beginPath();--%>
                        <%--ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);--%>
                        <%--ctx.fill();--%>
                    <%--}--%>
                    <%--requestAnimationFrame(snow);--%>
                <%--};--%>

                <%--function reset(flake) {--%>
                    <%--flake.x = Math.floor(Math.random() * canvas.width);--%>
                    <%--flake.y = 0;--%>
                    <%--flake.size = (Math.random() * 3) + 2;--%>
                    <%--flake.speed = (Math.random() * 1) + 0.5;--%>
                    <%--flake.velY = flake.speed;--%>
                    <%--flake.velX = 0;--%>
                    <%--flake.opacity = (Math.random() * 0.5) + 0.3;--%>
                <%--}--%>

                <%--function init() {--%>
                    <%--for (var i = 0; i < flakeCount; i++) {--%>
                        <%--var x = Math.floor(Math.random() * canvas.width),--%>
                            <%--y = Math.floor(Math.random() * canvas.height),--%>
                            <%--size = (Math.random() * 3) + 2,--%>
                            <%--speed = (Math.random() * 1) + 0.5,--%>
                            <%--opacity = (Math.random() * 0.5) + 0.3;--%>
                        <%--flakes.push({--%>
                            <%--speed: speed,--%>
                            <%--velY: speed,--%>
                            <%--velX: 0,--%>
                            <%--x: x,--%>
                            <%--y: y,--%>
                            <%--size: size,--%>
                            <%--stepSize: (Math.random()) / 30 * 1,--%>
                            <%--step: 0,--%>
                            <%--angle: 180,--%>
                            <%--opacity: opacity--%>
                        <%--});--%>
                    <%--}--%>
                    <%--snow();--%>
                <%--};--%>
                <%--document.addEventListener("mousemove", function (e) {--%>
                    <%--mX = e.clientX,--%>
                        <%--mY = e.clientY--%>
                <%--});--%>
                <%--window.addEventListener("resize", function () {--%>
                    <%--canvas.width = window.innerWidth;--%>
                    <%--canvas.height = window.innerHeight;--%>
                <%--});--%>
                <%--init();--%>
            <%--})();--%>
        <%--</script>--%>
        <%--<style>--%>
            <%--#Snow {--%>
                <%--position: fixed;--%>
                <%--top: 0;--%>
                <%--left: 0;--%>
                <%--width: 100%;--%>
                <%--height: 100%;--%>
                <%--z-index: 99999;--%>
                <%--background: RGBA(225, 225, 225, 0.1);--%>
                <%--pointer-events: none--%>
            <%--}--%>
        <%--</style>--%>
    <%--</div>--%>
</c:if>

<c:if test="${ frontPage.getProperty('body.template') != null && frontPage.getProperty('body.template').equals('empty')}">
    <sm:body />
    <%--<div style="background: red" id="vueapp">{{info}}</div>--%>
    <%--<script>--%>
        <%--new Vue({--%>
            <%--el:"#vueapp",--%>
            <%--data:{info:"hello vue and sitemesh2.4"}--%>
        <%--})--%>
    <%--</script>--%>
</c:if>



</body>
</html>

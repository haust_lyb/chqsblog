<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>春华秋实博客园</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/x-admin/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/x-admin/css/xadmin.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/plugins/jquery/jquery.3.2.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/x-admin/js/xadmin.js"></script>
    <link rel="stylesheet" href="<c:url value='/static/plugins/layui/css/layui.css'/> ">
    <script href='<c:url value='/static/plugins/layui/layui.js'/>'></script>

</head>
<body>
<div class="container">
    <div class="logo"><a href="./index.html">春华秋实博客园</a></div>
    <div class="left_open">
        <i title="展开左侧栏" class="iconfont">&#xe699;</i>
    </div>
    <ul class="layui-nav left fast-add" lay-filter="">
        <li class="layui-nav-item">
            <a href="javascript:;">快捷导航</a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a onclick="x_admin_show('慕课网','http://www.imooc.com')"><i class="iconfont">&#xe6a2;</i>慕课网</a></dd>
                <dd><a onclick="x_admin_show('百度搜索','http://www.baidu.com')"><i class="iconfont">&#xe6a8;</i>百度搜索</a></dd>
            </dl>
        </li>
    </ul>
    <ul class="layui-nav right" lay-filter="">
        <li class="layui-nav-item">
            <a href="javascript:;">admin</a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a onclick="x_admin_show('个人信息','http://www.baidu.com')">个人信息</a></dd>
                <dd><a onclick="x_admin_show('切换帐号','http://www.baidu.com')">切换帐号</a></dd>
                <dd><a href="${pageContext.request.contextPath}/admin/logout">退出</a></dd>
            </dl>
        </li>
        <li class="layui-nav-item to-index"><a href="${pageContext.request.contextPath}/">前台首页</a></li>
    </ul>

</div>
<!-- 顶部结束 -->
<!-- 中部开始 -->
<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>用户管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <%--<li>--%>
                        <%--<a _href="member-list.html">--%>
                            <%--<i class="iconfont">&#xe6a7;</i>--%>
                            <%--<cite>管理员管理</cite>--%>
                        <%--</a>--%>
                    <%--</li >--%>
                    <li>
                        <a _href="${pageContext.request.contextPath}/admin/customerManager">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>博主管理</cite>
                        </a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6ce;</i>
                    <cite>文章管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/admin/articleCategory">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>文章分类管理</cite>
                        </a>
                    </li>
                    <li>
                        <a _href="${pageContext.request.contextPath}/admin/articleManager">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>文章管理</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/admin/swiperManager">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>轮播管理</cite>
                        </a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b4;</i>
                    <cite>其他设置</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/admin/friendLinkManager">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>友情链接管理</cite>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->
<!-- 左侧菜单结束 -->
<!-- 右侧主体开始 -->
<div class="page-content">
    <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
        <ul class="layui-tab-title">
            <li class="home"><i class="layui-icon">&#xe68e;</i>我的桌面</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='${pageContext.request.contextPath}/admin/welcome' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="page-content-bg"></div>
</body>
</html>































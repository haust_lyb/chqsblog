<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/24
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>重新编辑</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/layui/css/layui.css">
    <script src='${pageContext.request.contextPath}/static/plugins/layui/layui.js'></script>
</head>
<body>

<div class="layui-container" style="padding: 20px;">
    <form  class="layui-form" action="${pageContext.request.contextPath}/admin/doEditFriendLink">

        <div class="layui-form-item" style="display: none;" >
            <label class="layui-form-label">uuid</label>
            <div class="layui-input-block">
                <input type="text" name="uuid" autocomplete="off" class="layui-input" value="${friendLink.uuid}">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">链接地址</label>
            <div class="layui-input-block">
                <input type="text" name="href" value="${friendLink.href}" required  lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">链接名称</label>
            <div class="layui-input-block">
                <input type="text" name="name" value="${friendLink.name}" required  lay-verify="required"  autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">是否上架</label>
            <div class="layui-input-block">
                <input type="radio" name="stat" value="0" title="下架" ${friendLink.stat==0?'checked':''}>
                <input type="radio" name="stat" value="1" title="上架"  ${friendLink.stat==1?'checked':''}>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn">确定</button>
            </div>
        </div>

    </form>

</div>


<script>
    layui.use(['table'],function () {
        var form=layui.form;
        var layer=layui.layer;
        var $=layui.jquery;

        <%--form.on('submit(formDemo)',function (data) {--%>
            <%--$.ajax({--%>
                <%--url:"${pageContext.request.contextPath}/admin/doEditFriendLink",--%>
                <%--method:'POST',--%>
                <%--data:JSON.stringify(data.field),--%>
                <%--success:function (res) {--%>
                    <%--layer.msg(res.msg);--%>
                    <%--// layer.close(layer.index);--%>
                <%--}--%>
            <%--})--%>
        <%--})--%>
    })
</script>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/10
  Time: 22:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/layui/css/layui.css">
    <script src='${pageContext.request.contextPath}/static/plugins/layui/layui.js'></script>
</head>
<body>
<h1 style="text-align: center">文章类别管理</h1>
<table class="layui-hide" id="test" lay-filter="demo"></table>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="test">测试</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>




<script>

    layui.use(['table','jquery'], function () {
        var table = layui.table;
        var $=layui.jquery;
        //显示信息列表
        $.ajax({
            url:"${pageContext.request.contextPath}/admin/getArticleContentTypeList",
            success:function (swi) {

                table.render({
                    elem: '#test'
                    // ,height: 315
                    // ,width:500
                    // ,url: '/demo/table/user/' //数据接口
                    , data: swi
                    , page: true //开启分页
                    , cols: [[ //表头
                        {field: 'uuid', title: 'id', sort: true}
                        , {field: 'typename', title: '类别名', sort: true}
                        , {fixed: 'right', width: 165, align: 'center', toolbar: '#barDemo'}
                    ]]
                });
            }
        })



        //监听工具条
        table.on('tool(demo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                , layEvent = obj.event; //获得 lay-event 对应的值
            if (layEvent === 'test') {
                // x_admin_show('慕课网',data.href);
                layer.open({
                    content:data.href,
                    type: 2,
                    area: [$(window).width()*0.9+"px", $(window).height()*0.9 +'px'],
                    fix: false, //不固定
                    maxmin: true,
                    shadeClose: true,
                    shade:0.4,
                    title: data.name
                })
                console.log(data.id);
            } else if (layEvent === 'del') {
                layer.confirm('真的删除行么', function (index) {
                    obj.del(); //删除对应行（tr）的DOM结构
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url:"${pageContext.request.contextPath}/admin/delArticleContentType?uuid="+data.uuid,
                        success:function (msgData) {
                            layer.msg(msgData.msg);
                        }
                    })
                });
            } else if (layEvent === 'edit') {
                layer.msg('编辑操作');
            }
        });

    });


</script>


</body>
</html>


<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/10
  Time: 22:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/layui/css/layui.css">
    <script src='${pageContext.request.contextPath}/static/plugins/layui/layui.js'></script>
    <style>
        .lyb-input{
            background: #fff;
            /*box-shadow: 1px 1px 5px 0px #f6f6f6;*/
            color: #000;
            width: 100%;
            height: 38px;

        }
    </style>
</head>
<body>
<h1 style="text-align: center;margin-top: 5px;" class="layui-bg-cyan">友情链接管理</h1>

<form style="padding: 20px;" class="layui-form" >
    <div class="layui-row">
        <div class="layui-col-md5"><input class="lyb-input" type="text" name="href" placeholder="请填写链接地址"></div>
        <div class="layui-col-md3"><input class="lyb-input" type="text" name="name"  placeholder="请填写链接名称"></div>
        <div class="layui-col-md2">
            <input type="submit" class="layui-btn layui-btn-danger" lay-submit lay-filter="addFriendLink" value="确认添加">
        </div>

    </div>



</form>



<table class="layui-hide" id="test" lay-filter="demo"></table>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="test">测试</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script type="text/html" id="statTemp">
    {{#  if(d.stat == 1){ }}
        上架
    {{#  } else { }}
        下架
    {{#  } }}

</script>
<%--<div id="laypage"></div>--%>


<script>
    function sleep(n,callback)
    {
        var start=new Date().getTime();
        while(true) if(new Date().getTime()-start>n){break;callback()} ;
    }

    layui.use(['table','jquery','form'], function () {
        var table = layui.table;
        var $=layui.jquery;
        var form=layui.form;
        var layer=layui.layer;
        //第一个实例


        function loaddata() {
            $.ajax({
                url:"${pageContext.request.contextPath}/admin/getFriendlyList",
                success:function (res) {

                    table.render({
                        elem: '#test'
                        // ,height: 315
                        // ,width:500
                        // ,url: '/demo/table/user/' //数据接口
                        , data: res
                        , page: true //开启分页
                        , cols: [[ //表头
                            {field: 'uuid', title: 'id', sort: true}
                            , {field: 'name', title: '友情链接名称', sort: true}
                            , {field: 'href', title: '真实地址', sort: true}
                            ,{field: 'stat', title: '是否上架', sort: true,templet: '#statTemp'}
                            , {fixed: 'right', width: 165, align: 'center', toolbar: '#barDemo'}
                        ]]
                    });
                }
            })
        }
        loaddata();


        //监听工具条
        table.on('tool(demo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                , layEvent = obj.event; //获得 lay-event 对应的值
            if (layEvent === 'test') {
                // x_admin_show('慕课网',data.href);
                layer.open({
                    content:data.href,
                    type: 2,
                    area: [$(window).width()*0.9+"px", $(window).height()*0.9 +'px'],
                    fix: false, //不固定
                    maxmin: true,
                    shadeClose: true,
                    shade:0.4,
                    title: data.name
                })
                console.log(data.id);
            } else if (layEvent === 'del') {
                layer.confirm('真的删除行么', function (index) {
                    obj.del(); //删除对应行（tr）的DOM结构
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url:"${pageContext.request.contextPath}/admin/delFridenlyLink?uuid="+data.uuid,
                        success:function (msgData) {
                            layer.msg(msgData.msg);
                            loaddata();
                        }
                    })
                });
            } else if (layEvent === 'edit') {
                layer.msg('编辑操作');

            }
        });



        //添加链接
        form.on('submit(addFriendLink)',function (data) {
            // layer.msg(JSON.stringify(data.field));
            $.ajax({
                url:"${pageContext.request.contextPath}/admin/addFriendLink",
                data:data.field,
                success:function (res) {
                    layer.msg(res.msg);
                    // loaddata();
                }
            })
            $(".lyb-input").val("");
            // sleep(1000,loaddata());
            loaddata();
            return false;
        })


    });


</script>


</body>







</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/4/14
  Time: 18:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
</head>
<body style="padding: 15px;">
<h1>添加文章</h1>
<script src="${pageContext.request.contextPath}/static/plugins/ueditor1.4.3.3/ueditor.config.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/ueditor1.4.3.3/ueditor.all.min.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/ueditor1.4.3.3/lang/zh-cn/zh-cn.js"></script>


<form action="${pageContext.request.contextPath}/admin/doAddArticle" method="post">

    <div class="form-group">
        <label for="title">uuid</label>
        <input type="text" name="uuid" value="${author.uuid}">
    </div>
    <div class="form-group">
        <label for="title">标题</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="标题">
    </div>
    <div class="form-group">
        <label for="description">概述</label>
        <input type="text" class="form-control" id="description" name="description" placeholder="概述">
    </div>
    <div class="form-group">
        <select name="typeUuid" id="">
            <option value="a864138e0eb248eb88f1d6df3c2febfb">java</option>
            <option value="5783f8a2ce9e4bd3b3254677121c4ad7">网页设计</option>
            <option value="6269659576e34ca39318143056c6e845">其他</option>
        </select>
    </div>
    <div class="checkbox">
        <label>

            <input type="radio" name="isPrivate" value="1">私有

            <input type="radio" name="isPrivate" value="0" checked="checked">公开
        </label>
    </div>

    <textarea id="myUeditor" name="content" type="text/plain">在这里编辑文章内容</textarea>


    <button type="submit" class="btn btn-block btn-success">提交</button>
</form>


<script type="text/javascript">
    var editor = UE.getEditor('myUeditor');
</script>
</body>
</html>

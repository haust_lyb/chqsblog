<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.0</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/static/x-admin/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/static/x-admin/css/xadmin.css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/static/plugins/jquery/jquery.3.2.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/static/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/static/x-admin/js/xadmin.js"></script>
    </head>
    <body>
    <div class="x-body">
        <blockquote class="layui-elem-quote">
            欢迎管理员：
            <span class="x-red">李一博</span>！当前时间：&nbsp;&nbsp;<span id="nowTime"></span>
        </blockquote>
        <fieldset class="layui-elem-field">
            <legend>数据统计</legend>
            <div class="layui-field-box">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                                <div carousel-item="">
                                    <ul class="layui-row layui-col-space10 layui-this">
                                        <li class="layui-col-xs2">
                                            <a href="javascript:;" class="x-admin-backlog-body">
                                                <h3>文章数</h3>
                                                <p>
                                                    <cite>66</cite></p>
                                            </a>
                                        </li>
                                        <li class="layui-col-xs2">
                                            <a href="javascript:;" class="x-admin-backlog-body">
                                                <h3>会员数</h3>
                                                <p>
                                                    <cite>12</cite></p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset class="layui-elem-field">
            <legend>系统信息</legend>
            <div class="layui-field-box">
                <table class="layui-table">
                    <tbody>
                        <tr>
                            <th>软件版本版本</th>
                            <td>chqsblog v1.1</td></tr>
                        <tr>
                            <th>服务器地址</th>
                            <td>chqsblog.cn</td></tr>
                        <tr>
                            <th>操作系统</th>
                            <td>winServer 2012</td></tr>
                        <tr>
                            <th>运行环境</th>
                            <td>tomcat8.5 jdk1.8 servlet3.5</td></tr>
                        <tr>
                            <th>MYSQL版本</th>
                            <td>5.1</td></tr>
                        <tr>
                            <th>上传附件限制</th>
                            <td>10M</td></tr>
                    </tbody>
                </table>
            </div>
        </fieldset>
        <fieldset class="layui-elem-field">
            <legend>开发者</legend>
            <div class="layui-field-box">
                <table class="layui-table">
                    <tbody>
                        <tr>
                            <th>版权所有</th>
                            <td>春华秋实博客园</td>
                        </tr>
                        <tr>
                            <th>开发者</th>
                            <td>李一博(1570194845@qq.com)</td></tr>
                    </tbody>
                </table>
            </div>
        </fieldset>
        <%--<blockquote class="layui-elem-quote layui-quote-nm">春华秋实博客园</blockquote>--%>
    </div>
        <script>

            document.getElementById("nowTime").innerHTML=new Date().toLocaleDateString()+new Date().toLocaleTimeString();
            setInterval(function () {
                var date=new Date();
                document.getElementById("nowTime").innerHTML=date.toLocaleDateString()+date.toLocaleTimeString();
            },1000)

        </script>
    </body>
</html>
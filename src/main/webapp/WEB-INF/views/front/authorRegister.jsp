<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>春华秋实博客园管理平台 登录</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/static/x-admin/css/font.css">
    <link rel="stylesheet" href="/static/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/static/plugins/jquery/jquery.3.2.1.min.js"></script>
    <script src="/static/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/x-admin/js/xadmin.js"></script>

</head>
<body class="login-bg" template="empty">

<div class="login">
    <div class="message">请设置您的密码</div>
    <div id="darkbannerwrap"></div>

    <form method="post" class="layui-form" action="/admin/check">
        <input type="text" style="display: none;" style="" name="key" value="${key}">
        <input name="firstPassWord" placeholder="请输入密码"  type="password" lay-verify="required" class="layui-input" >
        <hr class="hr15">
        <input name="passWord" lay-verify="required" placeholder="请再次输入密码"  type="password" class="layui-input">
        <hr class="hr15">
        <input value="确定" lay-submit lay-filter="formEditPassWord" style="width:100%;" type="submit">
        <hr class="hr20" >
    </form>
</div>

<script>

    layui.use(['layer','form'],function () {
        var layer=layui.layer;
        var form = layui.form;

        //监听提交
        form.on('submit(formEditPassWord)', function(data){
            // layer.msg(JSON.stringify(data.field)+typeof data+data);


            $.ajax({
                type:"POST",
                data:data.field,
                url:"/mailSender/checkRegister",
                success:function (msg) {
                    if (msg.code!=-1){
                        layer.msg(msg.msg);
                        setTimeout(function () {
                            window.location.href="${pageContext.request.contextPath}/front/home"
                        },1000);
                    }else {
                        layer.msg(msg.msg);
                        $(".layui-input").val("");
                        $(".layui-input:first").focus();
                    }

                }
            })
            return false;
        });
    })


</script>


</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/26
  Time: 16:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>我的博客</title>
    <script src="${pageContext.request.contextPath}/static/plugins/jquery/jquery.3.2.1.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
</head>
<body>
<script src="${pageContext.request.contextPath}/static/plugins/ueditor1.4.3.3/ueditor.config.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/ueditor1.4.3.3/ueditor.all.min.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/ueditor1.4.3.3/lang/zh-cn/zh-cn.js"></script>


<div class="layui-container" id="userForm" style="margin-top: 40px;">
    <div class="layui-row">
        <div class="layui-col-md3">
            <div style="padding: 10px;">
                <h3>我的博客</h3>
                <div id="myBlogsList" v-cloak>
                    <ul>
                        <li v-for="item in articles"><a :href="'${pageContext.request.contextPath}/front/showArticleById?uuid=' + item.uuid ">{{item.title}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="layui-col-md9">
            <div style="padding: 10px;">
                <div><h3>新建博客</h3></div>
                <div>
                    <form action="${pageContext.request.contextPath}/front/doAddArticle" method="post">
                        <div class="form-group">
                            <label for="title">标题</label>
                            <input type="text" class="form-control" autocomplete="off" id="title" name="title" placeholder="标题">
                        </div>
                        <div class="form-group">
                            <label for="description">概述</label>
                            <input type="text" class="form-control" autocomplete="off" id="description" name="description" placeholder="概述">
                        </div>
                        <div class="form-group">
                            <select name="typeuuid">
                                <option value="5c4d9138821a4e0297313100d1bd8368">html</option>
                                <option value="5c4d9138821a4e0297313100d1bd8366">Java</option>
                                <option value="5c4d9138821a4e0297313100d1bd8367">其他</option>
                            </select>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="radio" name="isprivate" value="1">私有
                                <input type="radio" name="isprivate" value="0" checked="checked">公开
                            </label>
                        </div>

                        <textarea id="myUeditor" name="content" type="text/plain">在这里编辑文章内容</textarea>


                        <button type="submit" class="btn btn-block btn-success">提交</button>
                    </form>


                    <script type="text/javascript">
                        var editor = UE.getEditor('myUeditor');
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>


<script>


    var myData={
        articles:[{
            "uuid": "46d6809e80e0494a8385ade4ddd1e1d6",
            "isprivate": 1,
            "createtime": 1527328694601,
            "lastedittime": 1527328694601,
            "authoruuid": "5c4d9138821a4e0297313100d1bd83a4",
            "typeuuid": "5c4d9138821a4e0297313100d1bd8366",
            "title": "hello",
            "content": "<p>veryGood</p>"
        }, {
            "uuid": "a5c0a8cae915499c8576cfff76a203f3",
            "isprivate": null,
            "createtime": 1527328932050,
            "lastedittime": 1527328932050,
            "authoruuid": "5c4d9138821a4e0297313100d1bd83a4",
            "typeuuid": null,
            "title": "finaltest",
            "content": "<h1 class=\"ue_t\" style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;text-align:center;margin:0px 0px 20px;\"><span style=\"color:#c0504d;\">[键入文档标题]</span></h1><p style=\"text-align:center;\"><strong class=\"ue_t\">[键入文档副标题]</strong></p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 1]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 2]</span></h3><p><span class=\"ue_t\" style=\"font-family:幼圆\"><img src=\"/upload/image/20180526/1527328930150081942.jpg\" title=\"1527328930150081942.jpg\" alt=\"pinglun.jpg\"/></span></p><p class=\"ue_t\" style=\"text-indent:2em;\">在“开始”选项卡上，通过从快速样式库中为所选文本选择一种外观，您可以方便地更改文档中所选文本的格式。 您还可以使用“开始”选项卡上的其他控件来直接设置文本格式。大多数控件都允许您选择是使用当前主题外观，还是使用某种直接指定的格式。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 3]</span></h3><p class=\"ue_t\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><p class=\"ue_t\"><br/></p><p><br/></p>"
        }, {
            "uuid": "af91e0c5d53c472698af71785adb7709",
            "isprivate": null,
            "createtime": 1527329020651,
            "lastedittime": 1527329020651,
            "authoruuid": "5c4d9138821a4e0297313100d1bd83a4",
            "typeuuid": null,
            "title": "什么？",
            "content": "<p>666</p>"
        }]
    }


    $.ajax({
        url:"${pageContext.request.contextPath}/front/getMyArticle",
        success:function (res) {
            myData.articles=res.object;
            console.log(typeof res.object);
        }
    })

    new Vue({
        el:"#myBlogsList",
        data:myData,
        methods:{

        }
    })
</script>

</body>
</html>

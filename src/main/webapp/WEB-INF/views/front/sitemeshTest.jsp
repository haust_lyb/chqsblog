<%--
  Created by IntelliJ IDEA.
  User: haiseer
  Date: 2018/4/26
  Time: 下午5:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="/static/plugins/swiper/zepto.min.js"></script>
</head>
<body>
<div class="layui-container-fluid" id="myVue1" style="margin-top: 80px;height: 500px;background: palevioletred;">
    <h1 style="text-align: center">vue.js sitemesh layui 相结合 </h1>
    <div v-for="item in testmsg" style="margin: 60px;font-size: 30px;">
        {{item.name}}:{{item.sex}}
    </div>
    <button id="myBtn" class="layui-btn layui-btn-success layui-btn-fluid" v-on:click="changeData">点击我就会改变数据，并且我就会消失
    </button>
</div>
<script>

    layui.use(['layer', 'jquery'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;

        function showMsg(msg) {
            layer.msg(msg);
        }

        var data = {
            testmsg: [
                {name: "小华", sex: "男"},
                {name: "小花", sex: "女"},
                {name: "小洋", sex: "女"}
            ]
        }
        new Vue({
            el: "#myVue1",
            data: data,
            methods: {
                changeData: function (event) {
                    showMsg("模拟分页改变数据");
                    this.testmsg = [
                        {name: "小狗", sex: "男"},
                        {name: "小猫", sex: "女"},
                        {name: "小羊", sex: "女"}
                    ];
                    $("#myBtn").hide();
                }
            }
        })
    })

</script>
</body>
</html>

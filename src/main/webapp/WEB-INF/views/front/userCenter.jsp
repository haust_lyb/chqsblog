<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/22
  Time: 15:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人中心</title>
    <style>
        body {
            background: #f6f6f6;
        }
    </style>
</head>
<body>

<div class="layui-container" id="userForm" style="margin-top: 40px;">
    <div class="layui-row">
        <div class="layui-col-md2" style="padding: 20px;">
            <div style="background: #fff;min-height: 500px;padding: 20px;display: flex;flex-direction: column;justify-content: flex-start;align-items: center;">
                <img src="<c:url value="/static/source/defaultHeadImg.jpg"/>"
                     style="border-radius: 50%;height:60px;height: 60px;box-shadow: 0px 0px 0px 4px #fff,0px 0px 0px 6px orangered;">

                <div style="color: #000;margin-top: 10px;font-size: 12px;">${sessionScope.author.loginname}</div>
                <div style="color: #000;margin-top: 10px;font-size: 12px;"><a href="${pageContext.request.contextPath}/front/myblog" class="layui-btn layui-btn-success">我的博客</a></div>
            </div>
        </div>
        <div class="layui-col-md10" style="padding: 20px;">


            <div style="background: #fff;padding: 20px;min-height: 500px;">
                <form action="/front/editUserCenter" method="post">

                    <div class="layui-form-item">
                        <label class="layui-form-label">昵称</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="nickname"value="${sessionScope.author.nickname}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">性别</label>
                        <div class="layui-input-block"  style="position: relative;top: 7px;">
                            男&nbsp;<input type="radio" name="sex" value="1" title="男" ${sessionScope.author.sex==1?'checked':''}>
                            &nbsp;&nbsp;
                            女&nbsp;<input type="radio" name="sex" value="0" title="女" ${sessionScope.author.sex==0?'checked':''}>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">电话</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="phone" value="${sessionScope.author.phone}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">年龄</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="age" value="${sessionScope.author.age}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">address</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="address" value="${sessionScope.author.address}">
                        </div>
                    </div>
                    
                    <%--昵称：<input type="text" name="nickname"value="${sessionScope.author.nickname}">--%>
                    <%--<br>--%>

                    <%--<input type="text"  name="uuid" value="${sessionScope.author.uuid}" style="display: none"><br>--%>

                    <%--男<input type="radio" name="sex" value="1" title="男" ${sessionScope.author.sex==1?'checked':''}>--%>
                    <%--女<input type="radio" name="sex" value="0" title="女" ${sessionScope.author.sex==0?'checked':''}><br>--%>

                    <%--电话：<input type="text" name="phone" value="${sessionScope.author.phone}"><br>--%>

                    <%--年龄：<input type="text" name="age" value="${sessionScope.author.age}"><br>--%>

                    <%--住址：<input type="text" name="address" placeholder="住址" value="${sessionScope.author.address}"><br>--%>
                    <%--<input type="submit" class="layui-btn layui-btn-success" value="提交">--%>
                    <%--&nbsp;&nbsp;--%>
                    <%--<input class="layui-btn layui-btn-success" type="reset">--%>


                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <input type="submit" class="layui-btn" />
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>

    layui.use(['jquery', 'element', 'form', 'table', 'layer'], function () {

        var $ = layui.jquery;
        var form = layui.form;

        var layer = layui.layer;
        <%--layer.msg("${sessionScope.author.loginname}");--%>

        // $(".layui-form-switch").click(function () {
        //     var enableEdit=$(".layui-form-switch em").html();
        //     console.log("可编辑开关:"+enableEdit);
        //     if(enableEdit=='on'){
        //         $("input").not("input:first").removeAttr("disabled");
        //     }else {
        //         $("input").not("input:first").attr("disabled");
        //     }
        // })


    });


</script>

</body>
</html>

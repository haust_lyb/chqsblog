<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/2
  Time: 23:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>春华秋实博客园</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/plugins/layui/css/layui.css">
    <script src='${pageContext.request.contextPath}/static/plugins/layui/layui.js'></script>
    <script src="${pageContext.request.contextPath}/static/plugins/jquery/jquery.3.2.1.min.js"></script>
    <style>

        body{
            background: #f6f6f6;

        }
    </style>
</head>
<body>
<div class="layui-container" style="background: #fff;margin-top: 60px;padding-bottom:20px;box-shadow: 2px 2px 5px 0px #f3f3f3">
    <div class="layui-row" >

        <div class="layui-col-md10 layui-col-md-offset1" >
            <h1 style="text-align: center">${articleitem.title}</h1>
            <div style="display: flex;justify-content: center;padding: 20px;">
                <span id="authorname" style="color: gray;font-size: 16px;margin-right: 20px;"></span>
                <span style="color: gray;font-size:16px;">
                <jsp:useBean id="Timestamp" class="java.util.Date"/>
                <c:set target="${Timestamp}" property="time" value="${articleitem.createtime}"/>
                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${Timestamp}" type="both"/>
                </span>
            </div>
            <div style="overflow: hidden;">
                ${articleitem.content}
            </div>
        </div>

    </div>



    <hr>
    <div class="layui-row">
        <div class="layui-col-md10 layui-col-md-offset1">
            <div style="border-radius: 5px;border: 1px solid #2F4056;padding: 10px;">
                评论：
                <hr>


                <c:if test="${fn:length(author.uuid) == 32}">
                    <div>
                        <form>
                            <textarea id="commenttextarea" name="" id="" cols="30" rows="10"style="width: 100%;height: 80px;">在这里添加评论</textarea>
                            <input class="layui-btn layui-btn-success" type="button" id="sendcomment" value="发表">
                        </form>
                    </div>
                </c:if>

                <div id="commentlistvue">
                    <div v-for="commentitem in object" style="padding: 10px;margin-top: 10px;border:1px dashed #f1a02f;">
                        <p>{{commentitem.content}}</p>
                        <p style="color: gray;text-align: right">{{getauthornamebyuuid(commentitem.fromauthor)}} &nbsp;&nbsp; {{timestampToTime(commentitem.createtime)}}</p>
                    </div>

                    <%--<div style="padding: 10px;margin-top: 10px;border:1px dashed #f1a02f;">--%>
                        <%--<p>文章不错，已收藏，感谢楼主！</p>--%>
                        <%--<p style="color: gray;text-align: right">小呆瓜 &nbsp;&nbsp; 2018-10-10 22:18:22</p>--%>
                    <%--</div>--%>
                </div>
            </div>
        </div>
    </div>

</div>

<script>

        var authoruuid="${articleitem.authoruuid}";
        var articleuuid="${articleitem.uuid}";
        var commentlist={
            object:[
                {"uuid":"692cf01a487a4f68be874f0abc2d7058","fromauthor":"5c4d9138821a4e0297313100d1bd83a4","createtime":1527392468356,"articleuuid":"9d07617d9c0f433dad1fa41d09b49333","content":"在这里添加评论"},
                {"uuid":"692cf01a487a4f68be874f0abc2d7058","fromauthor":"5c4d9138821a4e0297313100d1bd83a4","createtime":1527392468356,"articleuuid":"9d07617d9c0f433dad1fa41d09b49333","content":"在这里添加评论"}
            ]
        };

        new Vue({
            el:"#commentlistvue",
            data:commentlist,
            methods:{
                timestampToTime:function(timestamp) {
                    //var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
                    var date = new Date(timestamp);
                    Y = date.getFullYear() + '-';
                    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
                    D = date.getDate() + ' ';
                    h = date.getHours() + ':';
                    m = date.getMinutes() + ':';
                    s = date.getSeconds();
                    return Y+M+D+h+m+s;
                },
                getauthornamebyuuid:function (uuid) {
                    var nickname='佚名';
                    $.ajax({
                        async:false,//取消异步请求
                        url:"${pageContext.request.contextPath}/admin/getAuthorByUuid?authoruuid="+uuid,
                        success:function (res) {
                            console.log(res.nickname);
                            nickname=res.nickname;
                        }
                    });
                    return nickname;
                }
            }
        })

        function getCommentByArticleUuid() {
            $.ajax({
                url:"${pageContext.request.contextPath}/front/getCommentByArticleUuid?uuid="+articleuuid,
                success:function (res) {
                    commentlist.object=res.object
                }
            })
        }
        getCommentByArticleUuid();

        $.ajax({
            url:"${pageContext.request.contextPath}/admin/getAuthorByUuid?authoruuid="+authoruuid,
            success:function (res) {
                $("#authorname").html(res.nickname+"");
            }
        })


        $("#sendcomment").click(function () {
            var commentcomtext=$("#commenttextarea").val();

            $.ajax({
                url:"${pageContext.request.contextPath}/front/addComment?articleuuid=${articleitem.uuid}&&commentcomtext="+commentcomtext,
                success:function (res) {
                    if(res.code!=-1){
                        getCommentByArticleUuid();
                        $("#commenttextarea").val("")
                        alert(res.msg);
                    }else {
                        alert(res.msg);
                    }
                }
            })
        })

        // getCommentBuArticleUuid




</script>
</body>
</html>

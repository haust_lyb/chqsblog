<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/3
  Time: 22:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>聊天</title>
    <%--<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>--%>

    <style>
        body {
            background: url("/static/source/chatbg1.jpg") no-repeat;

        }
    </style>

</head>
<body>
<div class="layui-container" style="margin-top: 10vh;">

    <div class="layui-row" style="box-shadow: 2px 2px 2px #4E4E4E">
        <div style="background: url('/static/source/borderbg.jpg') repeat-x;height: 54px;width: 100%;">
            <h1 style="text-align: center;color: #fff;">

                春华秋实博客园-畅聊广场
            </h1>
        </div>
        <div style="opacity:0.9;overflow-y:auto;overflow-x:hidden;height:60vh;background: #4E4E4E;border:1px solid gray;padding: 20px;font-size: 24px;">

            <div id="msghistory">

                <%--<div style="">--%>
                    <%--<p style="font-size: 14px;padding: 10px;color: #000;font-weight: bold">--%>
                        <%--名字--%>
                    <%--</p>--%>
                    <%--<p>--%>
                        <%--<span style="display: inline-block;float: left;background: #B2E564;padding: 10px;border-radius: 10px;">消息消息消息</span>--%>
                    <%--<div style="clear: both"></div>--%>
                    <%--</p>--%>
                <%--</div>--%>
                <%--<div style="">--%>
                    <%--<p style="">--%>
                        <%--<span style="display: inline-block;float: right;font-size: 14px;padding: 10px;color: #000;font-weight: bold;border-radius: 10px;">消息消息消息</span>--%>
                    <%--<div style="clear: both"></div>--%>
                    <%--</p>--%>
                    <%--<p>--%>
                        <%--<span style="display: inline-block;float: right;background: #B2E564;padding: 10px;border-radius: 10px;">消息消息消息</span>--%>
                    <%--<div style="clear: both"></div>--%>
                    <%--</p>--%>
                <%--</div>--%>

            </div>

        </div>
    </div>

    <div class="layui-row" style="margin-top: 2vh;">
        <input type="text" style="font-size:24px;height:38px;float:left;background: #fff;width: 80%;" id="text"
               placeholder="在此输入文本" />
        <input style="width: 18%;height:38px;float: right" type="submit"
               class="layui-btn layui-btn-success layui-btn-fluid" value="发送" id="send">
    </div>


</div>


<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/plugins/jquery/jquery.3.2.1.min.js"></script>
<script>
    $("#text").focus();

    var user = {
        uuid: "${sessionScope.author.uuid}",
        nickname: "${sessionScope.author.nickname}",
        sex: "${sessionScope.author.sex}",
        headimg: "",
        text: ""
    }

    if ("${sessionScope.author.uuid}".length != 32) {
        user = {
            uuid: "",
            nickname: "游客",
            sex: "",
            headimg: "",
            text: ""
        }
    } else {
        console.log("Login");
    }




    var url = 'ws://' + "192.168.0.104:8080" + '/sock';
    var sock = new WebSocket(url);
    sock.onopen = function (ev) {
        console.log("opening...")
    }

    sock.onmessage = function (ev) {
        console.log("onmessage...")

        if($.parseJSON(ev.data).uuid!=user.uuid){
        // $.parseJSON(ev.data).uuid
        $("#msghistory").html($("#msghistory").html() + "" +
            "<div style=\"\">\n" +
            "                    <p style=\"font-size: 14px;padding: 10px;color: #000;font-weight: bold\">\n" +
            "                        "+$.parseJSON(ev.data).nickname+"\n" +
            "                    </p>\n" +
            "                    <p>\n" +
            "                        <span style=\"display: inline-block;float: left;background: #B2E564;padding: 10px;border-radius: 10px;\">"+$.parseJSON(ev.data).text+"</span>\n" +
            "                    <div style=\"clear: both\"></div>\n" +
            "                    </p>\n" +
            "                </div>")
        }else {
            $("#msghistory").html($("#msghistory").html() + "" +
                "<div style=\"\">\n" +
                "                    <p style=\"\">\n" +
                "                        <span style=\"display: inline-block;float: right;font-size: 14px;padding: 10px;color: #000;font-weight: bold;border-radius: 10px;\">"+$.parseJSON(ev.data).nickname+"</span>\n" +
                "                    <div style=\"clear: both\"></div>\n" +
                "                    </p>\n" +
                "                    <p>\n" +
                "                        <span style=\"display: inline-block;float: right;background: #B2E564;padding: 10px;border-radius: 10px;\">"+$.parseJSON(ev.data).text+"</span>\n" +
                "                    <div style=\"clear: both\"></div>\n" +
                "                    </p>\n" +
                "                </div>")
        }
    }

    document.onkeydown=function(){
        if (event.keyCode == 13){
            user.text=$("#text").val();
            sock.send(JSON.stringify(user));
            console.log("sendmsg..." + JSON.stringify(user));
            $("#text").val("");
            $("#text").focus();
        }
        else{

        }
    }

    $("#send").click(function () {
        user.text=$("#text").val();
        sock.send(JSON.stringify(user));
        console.log("sendmsg..." + JSON.stringify(user));
        $("#text").val("");
        $("#text").focus();
    })

</script>
</body>
</html>

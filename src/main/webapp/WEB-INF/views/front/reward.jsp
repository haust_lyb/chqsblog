<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: LiYibo
  Date: 2018/5/22
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>打赏</title>
</head>
<body>
<div class="layui-container" style="margin-top: 40px;">
   <div class="layui-row">
       <div class="layui-col-md12 layui-col-md-offset0" style="padding: 20px;">
           <div  style="padding: 20px;color: #fff;background: #009688;line-height: 48px;font-size: 21px;border-radius: 5px;">
               <p>欢迎大家来到春华秋实博客园！如果喜欢我们的园子，可以点击浏览器的收藏按钮添加收藏夹，如果想加入一起写博客，请点击上方登录即可！</p>
               <p>在这里提供一下本博客的源代码，内附数据库的sql文件，本项目是本人的毕业设计项目，<a style="color: #fcac6f" href="https://gitee.com/haust_lyb/chqsblog">您可以在码云中查看和下载完整的项目源码以及提出宝贵的意见</a>，如果该博客能够给您提供帮助，可以扫码打赏资助项目运营，金额不限，非常感谢！</p>
               <hr class="layui-bg-orange">
               <div style="display: flex;justify-content: space-around;">
                   <img src="<c:url value="/static/source/WeiXinPay.png"/> " style="width: 250px;height: 400px;">

                   <img src="<c:url value="/static/source/ZhiFuBao.jpg"/> " style="width: 250px;height: 400px;">
               </div>

               <hr class="layui-bg-orange">
               <p style="color: orangered">Sorry for not offer other pat method,but THANKS anyway!:)</p>
           </div>

           
       </div>
   </div>
</div>
</body>
</html>

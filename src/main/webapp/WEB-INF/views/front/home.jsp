<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/3/24
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head template="tab">
    <title>春华秋实博客园</title>
    <script src="${pageContext.request.contextPath}/static/plugins/jquery/jquery.3.2.1.min.js"></script>
    <style>
        body{
            background: #f6f6f6;
        }

        .articleblock:hover{
            background: #fcac6f;
        }


        .bg1{
            background: #009688;
        }

        .bg2{
            background: #FF5722;
        }

        .bg3{
            background: #5FB878;
        }

        .bg4{
            background: #00ffff;
        }

        .bg5{
            background: #FFB800;
        }
    </style>
</head>
<body style="background: #f6f6f6;">
<div class="layui-container-fluid">


    <div class="layui-container">
        <div class="layui-row layui-col-space10" style="background:#f3f3f3;margin-top: 39px;">
            <%--轮播--%>
            <div class="layui-col-md9" style="height: 280px;">
                <div>
                    <div class="layui-carousel" id="imgnews">
                        <div carousel-item>
                            <div class="lunboitem" url="http://localhost:8080/front/showArticleById?uuid=2956dcf2cf184c23b27e0845186530dd" style="position: relative;"><img style="width: 100%;height: 100%;"
                                                                  src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1527404605193&di=97cc62c882e59c5229de2f232cab5998&imgtype=0&src=http%3A%2F%2Fi1.sinaimg.cn%2Fedu%2F2015%2F1013%2FU12706P42DT20151013103926.jpg"></img>
                                <div style="position: absolute;bottom: 50px;left: 10px;height: 30px;"><span
                                        style="font-size: 25px;color: #fff;">Hibernate Search 入门配置及简单实用</span></div>
                            </div>
                            <div class="lunboitem" url="http://localhost:8080/front/showArticleById?uuid=718e827291ab415e971c269243b6cd8d" style="position: relative;"><img style="width: 100%;height: 100%;"
                                                                  src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1527404647473&di=1cd6c9a4325890dac0353bbc1021a505&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2F4d086e061d950a7b4a77b5ed01d162d9f2d3c95d.jpg"></img>
                                <div style="position: absolute;bottom: 50px;left: 10px;height: 30px;"><span
                                        style="font-size: 25px;color: #fff;">vue.js简介</span></div>
                            </div>
                            <div class="lunboitem" url="http://localhost:8080/front/showArticleById?uuid=9d07617d9c0f433dad1fa41d09b49333" style="position: relative;"><img style="width: 100%;height: 100%;"
                                                                  src="https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3946248267,1401482119&fm=27&gp=0.jpg"></img>
                                <div style="position: absolute;bottom: 50px;left: 10px;height: 30px;"><span
                                        style="font-size: 25px;color: #fff;">spring boot Junit单元测试_spring 普通项目单元测试</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--顶部左边的头像--%>
            <div class="layui-col-md3">
                <div class="layui-row" style="background:#fff;height: 280px;overflow: auto;position: relative;">
                    <div style="width:100%;height: 100%;
                background: #fff;display: flex;flex-direction: column;
                justify-content: space-between;align-self: center;align-items: center;">
                        <div style="flex: 0 1 220px;position:relative;width:100%;background: url('https://www.imsxm.com/wp-content/themes/Kratos-M/images/about.jpg');background-size: 100% 100%;"></div>
                        <div style="position:absolute;bottom: 60px;box-sizing: border-box;border-radius: 50%;
                    display: flex;flex-direction: column;align-items: center;border: 5px solid #fff;
                    flex: 0 1 80px;width: 80px;">
                            <div style="flex:0 1 70px;width: 70px;border-radius: 50%;
                    background: url('https://www.imsxm.com/wp-content/uploads/2016/09/cropped-9fc5dd51jw8f6qs0ax32wj20ro0rpq4p.jpg');
                    background-size:70px 70px;">
                            </div>
                        </div>
                        <div style="background: #fff;flex: 0 1 60px;position:relative;width: 100%;text-align: center;display: flex;flex-direction: column;justify-content: center;">
                            <strong style="font-size: 15px;">A Java Coder living in ShangHai</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-row layui-col-space10">

            <div class="layui-col-md9">
                <div style="background: #fff;">
                    <div style="font-size: 20px;height: 30px;line-height: 30px;padding:0px 10px;background: #000;color: #fff;">
                        上新博客
                    </div>
                    <div style="height: 2px;background: gray;margin-bottom: 10px;"></div>
                    <div class="layui-row layui-col-space10">


                        <div id="articleList"  v-cloak>
                            <div class="articleblock" style="background: #f6f6f6;margin: 20px;padding:10px;" v-for="articleitem in object"
                                 @click="showArticleDetial(articleitem.uuid)">
                                <p style="font-size: 24px;font-weight: bold;margin-bottom: 10px;">{{articleitem.title}}</p>
                                <p style="font-size:14 px;font-weight: bold;margin-bottom: 10px;">{{articleitem.description}}</p>
                                <p style="color: gray;font-size: 14px;">{{getauthornamebyuuid(articleitem.authoruuid)}}&nbsp;&nbsp;{{timestampToTime(articleitem.createtime)}}</p>
                            </div>
                        </div>
                        <%--<div class="layui-col-md12">--%>
                        <%--<div style="display: flex;flex-direction: column;justify-content: space-around;--%>
                        <%--background: #fff;border-bottom: dashed 1px gray;box-shadow:5px 5px 5px #f6f6f6;height:240px;width: 100%;">--%>
                        <%--<div style="display: flex;flex:1 0 200px; ">--%>
                        <%--<div style="flex: 0 0 300px;padding: 10px;"><img style="width: 100%;height: 100%"--%>
                        <%--src="https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=741360635,1658288474&fm=27&gp=0.jpg"--%>
                        <%--alt="chqsblog.cn"></div>--%>
                        <%--<div style="flex: 1 1 auto;padding: 10px;display: flex;flex-direction: column;justify-content: space-between;">--%>
                        <%--<div style="color: #000;font-size: 20px;"><h3>--%>
                        <%--什么是区块连--%>
                        <%--</h3>--%>
                        <%--<div style="color: gray;font-size: 15px;padding: 10px 0px;">--%>
                        <%--这个是功能很全的html5个人这个是--%>
                        <%--功能很全的html5个人博客模板,焦点图这个是功能很这个是功能很全的html5个人博客模板,焦点图--%>
                        <%--这个是功能很全的html5个人博客模板,焦点图全的html5个人博客模板,焦点图--%>
                        <%--博客模板,焦点图动态展示,增加了站内搜索,微信关注.侧栏使用了颜色标签,使...--%>
                        <%--</div>--%>
                        <%--</div>--%>

                        <%--<div style="display: flex;justify-content: flex-end;align-items:center;height: 20px;padding: 3px 0px;">--%>
                        <%--<div style="font-size: 12px;padding-right: 20px;color: gray;">李一博</div>--%>
                        <%--<div style="font-size: 12px;color:gray;">2010-10-10 20:20:15</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>


                    </div>

                </div>
            </div>
            <%--左侧的可切换tab--%>
            <div class="layui-col-md3">
                <div style="background: #fff;height:500px;padding:10px;">
                    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
                        <ul class="layui-tab-title">
                            <li class="layui-this">友情链接</li>
                            <li>联系作者</li>

                        </ul>
                        <div class="layui-tab-content">
                            <div class="layui-tab-item layui-show">
                                <div id="friendlist">
                                    <a v-for="item in friendlist" v-bind:class="getclass()" class="layui-btn" :href="item.href" style="margin-bottom: 10px;">{{item.name}}</a>
                                </div>

                            </div>
                            <div class="layui-tab-item" style="line-height: 20px;">
                                <p>
                                    您可以通过qq邮箱和我取得联系
                                </p>
                                <hr>
                                <p>1570194845@qq.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    var myData = {
        object: [
            {
                "uuid": "2956dcf2cf184c23b27e0845186530dd",
                "isprivate": 0,
                "createtime": 1527349451374,
                "lastedittime": 1527349451374,
                "authoruuid": "5c4d9138821a4e0297313100d1bd83a4",
                "typeuuid": "5c4d9138821a4e0297313100d1bd8367",
                "title": "Hibernate Search 入门配置及简单实用",
                "content": "<p>testData</p>"
            }
        ],
        friendlist:[
            {"uuid":"963ddae9d34c43008fee723d8cf07479","href":"http://sports.qq.com/nba/?ptag=baidu.ald.sc.nba","name":"腾讯nba","stat":1},
            {"uuid":"963ddae9d34c43008fee723d8cf07479","href":"http://sports.qq.com/nba/?ptag=baidu.ald.sc.nba","name":"腾讯nba","stat":1},
            {"uuid":"963ddae9d34c43008fee723d8cf07479","href":"http://sports.qq.com/nba/?ptag=baidu.ald.sc.nba","name":"腾讯nba","stat":1}
        ]
    };

    $.ajax({
        url:"${pageContext.request.contextPath}/front/getAllFriendLink",
        success:function (res) {
            myData.friendlist=res.object;
        }
    });
    new Vue({
        el:"#friendlist",
        data:myData,
        methods:{
            getclass:function () {
                var value=Math.floor(Math.random()*6 + 1);
                return "bg"+value+" layui-btn"
            }
        }
    });

    $.ajax({
        url: "${pageContext.request.contextPath}/front/getAllPublicArticle",
        success: function (res) {
            myData.object = res.object;
        }
    });

    $(".lunboitem").click(function () {
        window.location.href=$(this).attr("url");
    })

    new Vue({
        el: "#articleList",
        data: myData,
        methods: {
            showArticleDetial: function (uuid) {
                window.location.href = "${pageContext.request.contextPath}/front/showArticleById?uuid=" + uuid;
            },
            timestampToTime:function(timestamp) {
                //var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
                var date = new Date(timestamp);
                Y = date.getFullYear() + '-';
                M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
                D = date.getDate() + ' ';
                h = date.getHours() + ':';
                m = date.getMinutes() + ':';
                s = date.getSeconds();
                return Y+M+D+h+m+s;
            },
            getauthornamebyuuid:function (uuid) {
                var nickname='佚名';
                $.ajax({
                    async:false,//取消异步请求
                    url:"${pageContext.request.contextPath}/admin/getAuthorByUuid?authoruuid="+uuid,
                    success:function (res) {
                        console.log(res.nickname);
                        nickname=res.nickname;

                    }
                });
                return nickname;
            }
        }
    });
</script>

</body>

</html>
